from setuptools import setup, find_packages

import imp 

version = imp.load_source('mapcut.version', 'mapcut/version.py')

with open('README.md', 'r') as f:
    long_description = f.read()


setup(
    name='mapcut',
    version= version.version,
    description='Python module for reaction simulation postprocessing',
    author='Yanze Wu',
    author_email='wuyanze@sjtu.edu.cn',
    packages=find_packages(),
    long_description=long_description,
    long_description_content_type='text/markdown',
    classifiers=[
        "Programming Language :: Python :: 3"
    ],
    install_requires=[
        'numpy >= 1.13.0',
        'scipy >= 0.19.0',
        'pandas >= 0.22.0',
        'networkx >= 2.1.0'
    ]
)