
""" Top command receiver, and generate command to low-level scripts.
"""

import sys

from . import version
from .stats import merge, diff
from .stats import rateconst
from .stats import sample

from .util import errhandle

def main(args):

    version_str = 'Current Mapcut version: %s' % version.version
    help_str = '''
SYMNOPSIS:
mapcut [COMMAND] [ARGS...]

COMMANDS:
mapcut diff [args...]
mapcut merge [args...]
mapcut rate [args...]
mapcut reduce [args...]
mapcut run [args...]
mapcut sample [args...]
mapcut show [args...]

Please type 'mapcut [command] -h' to get help of a specific command.'''

    try:

        if len(args) == 1:
            print('Please specify a sub-command.')
            print(help_str)
            return 0

        if args[1] == 'diff':
            return diff._main(args[1:])

        elif args[1] == 'merge':
            return merge._main(args[1:])

        elif args[1] == 'rate':
            return rateconst._main(args[1:])

        elif args[1] == 'sample':
            return sample._main(args[1:])

        elif args[1] == 'reduce':
            from . import ui_reduce
            return ui_reduce.main(args[1:])

        elif args[1] == 'run':
            from . import ui_run
            return ui_run.main(args[1:])

        elif args[1] == 'show':
            from . import ui_graph
            return ui_graph._main(args[1:])

        elif args[1] == '--help' or args[1] == '-h':
            print(help_str)
            return 0

        elif args[1] == '--version' or args[1] == '-v':
            print(version_str)
            return 0

        else:
            print('Error: arg %s not recognized. Please use "-h" to see helps' % args[1], file=sys.stderr)
            return 1

    except Exception as e:
        print(errhandle.translate_ex(e), file=sys.stderr)
        return 1

if __name__ == '__main__':
    main(sys.argv)
