""" Handles raw data of reaxff analysis.
"""

import pandas
import numpy as np
from ..core import ReaxSample
from ..util import reaction


class ReaxRawData:
    """ Stores raw reaction simulation data (reaction rates/concentrations).
    A `ReaxRawData` object contains two tables: Concentration table and reaction rate table,
    both of which are `pandas.DataFrame` objects.

    Example:

    ```
    > with open('conc.csv') as f_fonc:   # concentration table
        f_conc.write('t,a,b,c\n1,1.0,0.5,0.0\n2,0.9,0.4,0.1')
    > with open('rate.csv') as f_rate:   # rate table
        f_rate.write('t,a+b=c\n1,0.0\n2,0.1')
    > from mapcut import stats
    > rawdata = stats.dump.read_file('conc.csv', 'rate.csv')
    (ReaxRawData with 3 species and 1 reactions, total 2 frames)
    > rawdata.t()    # times
    array([1, 2], dtype=int64)
    > rawdata['a']
    array([1., 0.9])
    > rawdata['c=a+b']
    array([-0., -0.1])
    > rawdata[0:1]
    (ReaxRawData with 3 species and 1 reactions, total 1 frames)
    ```
    """

    def __init__(self, c_dump=None, r_dump=None, include_reverse=True, **kwargs):
        self.c_dump = c_dump
        self.r_dump = r_dump
        self.include_reverse = include_reverse

        return super().__init__(**kwargs)

    def __len__(self):
        return len(self.c_dump)

    def __repr__(self):
        return '(ReaxRawData with %d species and %d reactions, total %d frames)' % (
                len(self.c_dump.columns) if self.c_dump is not None else 0,
                len(self.r_dump.columns) if self.r_dump is not None else 0,
                len(self.c_dump)
            )
        
    def __getitem__(self, indexer):
        if isinstance(indexer, slice):
            return ReaxRawData(self.c_dump.iloc[indexer], self.r_dump.iloc[indexer], include_reverse=self.include_reverse)
        elif isinstance(indexer, str):
            if reaction.EQU_SYMBOL in indexer:
                return self.index_reaction(indexer)
            else:
                return self.concentration(indexer)
        else:
            raise ValueError(indexer)

    def _validity_check(self):
        # check validity
        assert len(self.r_dump) == len(self.c_dump), 'Sample length not match'
        assert (self.r_dump.index == self.c_dump.index).all(), 'Time stamp not match'
        assert len(self.species()) == len(set(self.species())), 'Species repeat'
        assert len(self.reactions()) == len(set(self.reactions())), 'Reactions repeat'
        assert set(self.species()).issuperset(reaction.species_set(self.reactions())), 'Reaction has unknown species'

    def canon_reaction_names(self):
        """ Canonicalize reaction names.
        """
        # determine if there is reverse
        new_name_and_is_reverse = [reaction.canon_reaxstr(r, True) for r in self.r_dump.columns]
        new_names = [n[0] for n in new_name_and_is_reverse]

        self.include_reverse = (len(set(new_names)) != len(new_names))

        if self.include_reverse:  # if include reverse, just change name partially
            new_columns = [reaction.canon_reaxstr_without_reverse(r) for r in self.r_dump.columns]
        
        else:                   # otherwise change the symbol of rate data
            new_columns = new_names 
            for i, is_reverse in enumerate([n[1] for n in new_name_and_is_reverse]):
                if is_reverse:
                    self.r_dump.iloc[:, i] *= -1

        self.r_dump.columns = new_columns

    def concentration(self, species_name):
        """ Get concentration of a specific species.

        Args:
            species_name: `str`, name of species.

        Returns:
            `numpy.array` of species concentration during the process.
        """
        return self.c_dump[species_name].as_matrix()

    def index_reaction(self, reaction_name):
        """ Return rate of a specific reaction.

        Args:
            reaction_name: `str`, name of reaction, will be automatically canonicalized.

        Returns:
            `numpy.array` of reaction rates during the process.
        """
        if self.include_reverse:
            name_1 = reaction.canon_reaxstr_without_reverse(reaction_name)
            for rxn in self.r_dump.columns:
                if name_1 == reaction.canon_reaxstr_without_reverse(rxn):
                    return self.rate(rxn)
        else:
            name_1, is_reverse_1 = reaction.canon_reaxstr(reaction_name, True)
            for rxn in self.r_dump.columns:
                name_2, is_reverse_2 = reaction.canon_reaxstr(rxn, True)
                if name_1 == name_2:
                    return self.rate(rxn) * (-1 if is_reverse_1^is_reverse_2 else 1)
        raise IndexError(reaction_name)

    def init_concentration(self, volume=1.0, atol=0.0, rtol=1e-10):
        """ Returns the initial concentration of each species.
        
        Args:
            volume: scale coefficient.
            atol, rtol: `float`, species with concentration lower than `atol+Max*rtol` will be ignored.
                where `Max` is the maximum value of concentration at the beginning.

        Returns:
            init_c: `dict` containing initial concentration {species:concentration}
        """
        init_c = {}
        init_row = self.c_dump.iloc[0:1]
        maxc = np.max(init_row.as_matrix())
        for s in self.c_dump.columns:
            if float(init_row[s]) > atol + abs(maxc)*rtol:
                init_c[s] = float(init_row[s]) /volume
        return init_c        

    def integral_dev(self, rawdata_ref, species_name, weight=None):
        """ Returns the integral deviation of species concentration.
            Dev = \sum_{s \in species_name}{\int{|c-c_ref|/c_ref*c_ref' dt} * weight}}

        Args:
            rawdata_ref: Reference of `ReaxRawData` instance.
            species_name: Target species names.
            weight: `list` of weight of each species, or `None` (set all weight to 1)
            
        Returns:
            idev: `float`, the integral deviation of species concentration compared to reference `ReaxRawData`.

        Note:
            This is originally proposed by Valorani et al. (ECCFD 2006). Here we slightly modified it to make
            it always positive.
        """
        weight = [1.0] * len(species_name) if weight is None else weight

        assert len(weight) == len(species_name), 'Weight does not have same length as species'

        def numdiff(a): # numerical differentiation
            d = np.convolve(a, [0.5, 0, -0.5], 'same')
            d[0] = a[1] - a[0]
            return d

        return sum([
            w * np.sum(
                np.abs(self.c_dump[s] - rawdata_ref.c_dump[s])/rawdata_ref.c_dump[s] * np.abs(numdiff(rawdata_ref.c_dump[s]))
                ) / np.max(rawdata_ref.c_dump[s]) for s, w in zip(species_name, weight)
            ])

    def rate(self, reaction_name):
        """ Get rate of a specific reaction. This is the standard version of 
            ``index_reaction()``, which only accepts canonicalized reaction name.

        Args:
            reaction_name: `str`, reaction name must be exactly same as recorded.

        Returns:
            `numpy.array`` of reaction rates over time.
        """
        return self.r_dump[reaction_name].as_matrix()

    def reactions(self):
        """ Return `list` of reaction names.
        """
        return self.r_dump.columns.tolist()

    def sample(self, sample_int, sample_range=None, volume=1.0):
        """ Sample from `ReaxRawData`.

        Args:
            sample_int: `int`, sample interval as frame
            sample_range: `int`, sample range as frame (None to be same as sample_int)
            volume: `float`, system volume. The concentration output will be scaled
                by a factor of ``1/volume``.
        
        Returns:
            `ReaxSample` instance.
        """

        if sample_range is None:
            sample_range = sample_int
        assert sample_int > 0 and sample_range > 0, 'Invalid sample argument'

        # init
        data_c = []
        data_r = []

        init_c = self.init_concentration(volume=volume)

        timestep = self.c_dump.index[1] - self.c_dump.index[0]
        times = []

        for i in range(0, len(self.c_dump) - sample_range + 1, sample_int):
            actual_range = min(sample_range, len(self.c_dump) - i)
            data_c.append(
                self.c_dump.iloc[i:i+actual_range].mean().as_matrix() / volume, # mean concentration
            )
            times.append((i + actual_range/2) * timestep)

            reaction_sample = self.r_dump.iloc[i:i+actual_range].mean().as_matrix() / (volume * timestep)
            data_r.append(reaction_sample)                              # mean reaction rate

        sample_c = pandas.DataFrame(data_c, index=times, columns=self.c_dump.columns)
        sample_r = pandas.DataFrame(data_r, index=times, columns=self.r_dump.columns)
        
        return ReaxSample(sample_c, sample_r, init_c, include_reverse=self.include_reverse)

    def set_condition(self, timestep=1.0, volume=1.0):
        """ Scale the timestep and volume of the system.
        """
        self.c_dump /= volume
        self.r_dump /= (volume * timestep)

    def species(self):
        """ Return `list` of species.
        """
        return self.c_dump.columns.tolist()

    def t(self):
        """ Return `numpy.array`` of sample times.
        """
        return self.c_dump.index.values


def read_file(filename_c, filename_r, verbose=True, norm_reaction=True, nrows=None):
    """ Read concentration and reaction raw data files.
    The raw data files are basically two separated spreadsheet files, one recording concentration,
    and the other recording reaction rates.

    Args:
        filename_c: `str`, filename of concentration table.
        filename_r: `str`, filename of reaction rate table.
        verbose: `bool`, print things while reading.
        norm_reaction: `bool`, canonicalize reaction names read.
        nrows: `int`, only read first N rows of the tables.

    Returns:
        `ReaxRawData` instance.
    """
    raw_data = ReaxRawData()
    raw_data.c_dump = pandas.read_csv(filename_c, index_col=0, nrows=nrows)
    raw_data.r_dump = pandas.read_csv(filename_r, index_col=0, nrows=nrows)

    if norm_reaction:
        raw_data.canon_reaction_names()

    raw_data._validity_check()

    if verbose:
        print(raw_data)
    return raw_data

