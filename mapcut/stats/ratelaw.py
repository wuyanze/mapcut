
import csv
import numpy as np

from ..util import reaction
from ..util.parse import parse_dict
from ..core import reaxmap
from .dump import ReaxRawData


def _add_np_array(a, b, offset):
    """ add b to a with offset
    """
    ret = np.zeros(max(len(a), len(b) + offset))
    ret[:len(a)] = a 
    ret[offset:] += b 
    return ret 


def _resample(x, nstep):
    return x[::nstep]


def _select_last_turning_point(x, y, atol=0.03, rtol=0.1, nseg=100):

    assert len(x) == len(y), 'Length not match'
    x_ = _resample(x, len(x)//nseg)
    y_ = _resample(y, len(y)//nseg)
    k = (y_[-1] - y_[:-1]) / (x_[-1] - x_[:-1])
    k[x_[:-1] == x_[-1]] = 0.0

    dev = np.zeros_like(y_)
    dev[-1] = 0.0
    for i in range(len(k)):
        e = y_[i:] - y_[i] - k[i] * (x_[i:] - x_[i])
        dev[i] = np.sqrt(np.average(e*e))
    dev = dev / np.max(y_)

    tol = atol + rtol * np.max(dev)
    _compare = dev < tol 

    return np.argmax(_compare) * int(len(x)//nseg) if _compare[-1] == True else len(x)


def _is_proportional(x, y, atol, rtol, nseg):

    assert len(x) == len(y), 'Length not match'
    x_ = _resample(x, len(x)//nseg)
    y_ = _resample(y, len(y)//nseg)
    k = y_[-1] / x_[-1]

    tol = atol + rtol * np.max(y_)
    return np.std(y_ - k*x_) < tol 


def _remove_redundant_data(x, y):
    
    sel = np.zeros(len(x), dtype=bool)
    sel[0] = True
    np.logical_or(x[1:] != x[:-1], y[1:] != y[:-1], out=sel[1:])
    return x[sel], y[sel]


def _rate_constant(cu_rate_data, cu_conc_product, method):
    
    if cu_rate_data[-1] == 0:
        return 0.0, 0.0

    # remove repeat points
    _cu_conc_product, _cu_rate_data = _remove_redundant_data(cu_conc_product, cu_rate_data)

    if method == 'div':
        result = _cu_rate_data[-1] / _cu_conc_product[-1]
        res = np.sum(np.abs(_cu_rate_data - result * _cu_conc_product))
        return result, res / np.sum(_cu_rate_data)
    elif method == 'fit':
        result, res = np.polyfit(_cu_conc_product, _cu_rate_data, 1, full=True)[0:2]
        return result[0], res[0] / np.sum(_cu_rate_data)
    elif method == 'fit0':
        result, res = np.linalg.lstsq(_cu_conc_product[:, np.newaxis], _cu_rate_data)[0:2]
        return result[0], res[0] / np.sum(_cu_rate_data)
    else:
        raise ValueError(method)


def rate_constant(rawdata, reaction_name, slice_=None, method='div'):
    """ Perform a quick calculation of rate constant by fitting.

    Args:
        rawdata: `ReaxRawData` instance;
        reaction_name: `str`, name of the reaction;
        slice_: `slice` object;
        method: can be ``div``, ``fit``, ``fit0`` (which fixes intercept to 0).
    
    Returns:
        rate constant: `float`.
        residual: relative residual to sum of rate of the fitting.
    """
    assert rawdata.include_reverse, 'Must have forward and backward reaction'
    assert rawdata.r_dump is not None, 'Cannot calculate without reaction data'

    rate_data = rawdata.index_reaction(reaction_name)  
    reactants = reaction.split_reaxstr(reaction_name)[0]
    if slice_ is not None:
        rate_data = rate_data[slice_]
        conc_product = np.product([rawdata.concentration(s)[slice_] for s in reactants], axis=0)
    else:
        conc_product = np.product([rawdata.concentration(s) for s in reactants], axis=0)

    return _rate_constant(np.cumsum(rate_data), np.cumsum(conc_product), method=method)


def rawdata2reaxmap(rawdata, method='div'):
    """ Simple interface calculating rate constants by invoking `rate_constant()`.
    For a comprehend and tuneable calculation, please use `ReaxStat.to_reaxmap()`.

    Args:
        rawdata: `ReaxRawData` instance;
        method: Can be ``div``, ``fix`` or ``fix0``, see `rate_constant()` for details.
    
    Returns:
        rmap: `ReaxMap` instance.
    """
    rmap = reaxmap.ReaxMap(species=rawdata.species())
    for rxn in set([reaction.canon_reaxstr(r) for r in rawdata.reactions()]):
        kp = rate_constant(rawdata, rxn, method=method)[0]
        kc = rate_constant(rawdata, reaction.rev_reaxstr(rxn), method=method)[0]

        rmap.add_reaction(rxn, (kp, kc))

    return rmap 


class ReaxStat:
    """ Object handling rate constant calculation from rate statistics. 
    
    Example:

    ```
    > with open('conc.csv') as f_fonc:   # concentration table
        f_conc.write('t,a,b\n0,10,0\n1,9,1\n2,8,2')
    > with open('rate.csv') as f_rate:   # rate table
        f_rate.write('t,a=b,b=a\n0,0,0\n1,1,0\n2,1,0')
    > from mapcut import stat, ReaxStat
    > rawdata = stats.dump.read_file('conc.csv', 'rate.csv') # rawdata object
    (ReaxRawData with 2 species and 2 reactions, total 3 frames)
    > rawdata['a=b']
    array([0, 1, 1], dtype=int64)
    > rawdata['b=a']    # forward and backward reaction is different now
    array([0, 0, 0], dtype=int64)
    > rs = ReaxStat()
    > rs.load(rawdata)
    > rs.submit()
    > rmap = rs.to_reaxmap(volume=1.0, stat_cutoff=0, sample_cutoff=0)
    > rmap
    (reaxmap with 2 species and 1 reactions)
    > rmap.toarray()[2:4]    # rate constant of a=b and b=a
    (array([0.07407407]), array([0]))
    ```
    """
    def __init__(self):
        self.timestep = None
        self.rawdatas = []      # rawdata, offset
        self.reactions = dict() # dict of cumsum of reaction concentration

    def load(self, rawdata, add='para'):
        """ Load `ReaxRawData` object for statistics. This method can be called
        more than one time, and if not called at the first time, the new data
        will be overlayed according to the `add` option.

        Args:
            rawdata: `ReaxRawData` object, with forward and backward reaction separated.
            add: `str`, can be ``para`` or ``seq``.
                para: Overlay data parallel.
                seq: Overlay data sequentially, i.e. put new data behind.
        
        Note:
            In order to calculate rate constant by `to_reaxmap()`, rawdata here must be 
            number of species (undivided by volume). Otherwise some of the error estimation
            function may not be working properly.

            After finished loading, `submit()` should be called to finish the data.
        """
        assert rawdata.include_reverse, 'Cannot perform statistics without reverse'
        assert add in ('para', 'seq'), 'Invalid option'

        if self.timestep is None:
            self.timestep = rawdata.c_dump.index[1] - rawdata.c_dump.index[0]
        else: # already load a data
            assert self.timestep == rawdata.c_dump.index[1] - rawdata.c_dump.index[0], 'Timestep not match'

        self.rawdatas.append((rawdata, 0 if add == 'para' else len(rawdata)))
    
    def rate_constant(self, reaction_name, slice_=slice(None), method='div'):
        """ Calculate rate constant of specified reaction, by using all data or simply
        a part of the data.

        Args:
            reaction_name: `str`, name of the reaction.
            slice_: `slice` object choosing which part of the data.
            method: Can be ``div``, ``fix`` or ``fix0``, see `rate_constant()` for details.

        Returns:
            `float`, rate constant of a specific reaction.
        """

        reaction_name = reaction.canon_reaxstr_without_reverse(reaction_name)
        name_1 = reaction.canon_reaxstr(reaction_name)
        if name_1 == reaction_name:
            return _rate_constant(self.reactions[name_1][0][slice_], self.reactions[name_1][2][slice_], method=method)
        else:
            return _rate_constant(self.reactions[name_1][1][slice_], self.reactions[name_1][3][slice_], method=method)

    def submit(self):
        """ Called after loading all the data.
        """

        tot_reactions = set()
        for rawdata, add in self.rawdatas:
            tot_reactions.update([reaction.canon_reaxstr_without_reverse(r) for r in rawdata.reactions()])

        for r in tot_reactions:
            self.reactions[r] = [np.array([]), np.array([])]
            reactants = reaction.split_reaxstr(r)[0]

            for rawdata, offset in self.rawdatas:

                # rate forward
                self.reactions[r][0] = _add_np_array(self.reactions[r][0], rawdata.rate(r), offset)

                # concentration product forward
                try:
                    conc_product_reactants = np.product([rawdata.concentration(s) for s in reactants], axis=0)
                except KeyError:
                    conc_product_reactants = np.zeros(len(rawdata))

                self.reactions[r][1] = _add_np_array(self.reactions[r][1], conc_product_reactants, offset)

    def to_reaxmap(self, volume, stat_cutoff=3, order_cutoff=4, sample_cutoff=10, sample_corr_cutoff=None, 
        corr_detect=False, ignore_front=False, full=False, **kwargs):
        """ Construct a `ReaxMap` object, with all rate constants calculated. To fulfill the power of this
        function for raw simulation data, the `ReaxRawData` fed must be a "raw" sample, where the 
        concentration and reaction rates has not scaled by volume. Some of the reaction will be "removed",
        which means they will not appear in the `ReaxMap` output, but their information will still be recorded
        and outputed in `reaction_info`.

        Args:
            volume: `float`, system volume;       
            stat_cutoff: Remove reactions that happens less than this time. Set 0 to disable.
            order_cutoff: Remove reactions with number of *reactants* more than this value.
            sample_cutoff: Remove reactions with sample product less than this number. Set 0 to disable.
            sample_corr_cutoff: If not `None`, keep reaction with sample product less than this number,
                but happens more than this cutoff;
            corr_detect: Automatically construct bimolecular reaction with strong correlation of 
                forward and backward reactions. Set `False` to disable.
            ignore_front: Only use the later part for statistics if rate constant are inconsistent (for each reaction).
                Set `False` to disable.
            full: Output reaction information as a `dict`.

        Kwargs:
            keep_removed: Still keep removed reactions in reaxmap (though rate constants will be 0).
            method: Can be "div" (default), "fix" or "fix0", see `rate_constant()` for details.
            slice_: `slice`, selection of data. If given, `ignore_front` will be ignored.
            nseg: Segment number used in detecting the turning point (fast stage) of reaction rate.
            autoslice_atol, autoslice_rtol: `float`, tolerance for autoslice detection. Default is 0.03 and 0.1.
            corr_atol, corr_rtol: `float`, tolerance for correlation detection. Default is 0.0 and 0.02.

        Returns:
            rmap: `ReaxMap` instance;
            reaction_info: Only output when `full` is set. It is a `dict` of `dict`, storing information of reactions.
            The entries of first level of dict is forward reaction names. The entries of second level of dict is
            as following:
                select: `bool`, whether the reaction is selected in reaxmap.
                info: `str`, describe the reason why a reaction is not selected.
                sample_begin, sample_end: range index of data used.
                k: Rate constant of forward reaction.
                res: Residual of rate constant calculation.
                rate_sum_tot, rate_sum_used: Overall and used integral of reaction rate.
                conc_sum_tot, conc_sum_used: Overall and used integral of species concentration product.
                sample_flux: Used time integral of concentration product.
                corr: `bool`, describe whether forward and backward reaction is strongly coupled.
                back_ratio: if `corr` is `True`, describe the percentage of backward reaction in forward reaction.
        """

        keep_removed = kwargs.get('keep_removed', False)
        method = kwargs.get('method', 'div')
        slice_ = kwargs.get('slice_', None)
        nseg = kwargs.get('nseg', 100)

        autoslice_atol = kwargs.get('autoslice_atol', 0.03)
        autoslice_rtol = kwargs.get('autoslice_rtol', 0.1)

        corr_atol = kwargs.get('corr_atol', 0.0)
        corr_rtol = kwargs.get('corr_rtol', 0.02)

        reaction_info = {}

        for rxn, (rate, conc_product) in self.reactions.items():
            
            reactants, products = reaction.split_reaxstr(rxn)

            # cutoff

            reaction_info[rxn] = {
                'select': True,     # is selected
                'sample_begin': 0.0, # begin position of sample (in time format)
                'sample_end': len(rate) * self.timestep,
                'corr': False,      # is correlated with reverse reaction (only for single-atom reaction)
                'k': 0.0,           # rate constant
                'res': 0.0,         # relative residual
                'rate_sum_tot':np.sum(rate),
                'rate_sum_used':0,
                'conc_sum_tot':np.sum(conc_product),
                'conc_sum_used':0,
                'sample_flux': 0.0, # total sample number * timestep
                'info': ''          # information string
                }

            if np.sum(rate) == 0:
                reaction_info[rxn].update({
                    'select': False,
                    'info': 'No reaction detected'
                })
                continue 

            ret, info = self._check_reaction(rate, conc_product, reactants, 
                stat_cutoff=stat_cutoff,
                order_cutoff=order_cutoff,
                sample_cutoff=sample_cutoff,
                sample_corr_cutoff=sample_corr_cutoff)

            if ret == False:
                reaction_info[rxn].update({
                    'select': False,
                    'info': info 
                })
                continue 

            cu_rate = np.cumsum(rate)
            cu_conc_product = np.cumsum(conc_product)

            scale = kwargs.get('scale', 1.0)

            # if strong correlation

            if corr_detect:

                if (len(reactants) == 2 and len(products) == 1) or (len(reactants) == 1 and len(products) == 2):
                    
                    rev_rxn = reaction.rev_reaxstr(rxn)
                    rev_viewed = False 

                    if rev_rxn in reaction_info:
                        if reaction_info[rev_rxn]['corr'] == True: # already has strong correlation
                            back_ratio = 1.0/reaction_info[rev_rxn]['back_ratio']
                            reaction_info[rxn].update({
                                'corr': True,
                                'info': 'Strong correlation with reverse reaction',
                                'back_ratio': back_ratio
                            })
                            if back_ratio < 1.0: # backward > forward
                                scale *= (1.0 - back_ratio)
                            else:           # forward > backward, already written into k
                                continue 
                            
                    else:

                        has_corr, back_ratio = self._is_strong_correlation(
                            cu_rate,
                            np.cumsum(self.reactions[reaction.rev_reaxstr(rxn)][0]),
                            cu_conc_product,
                            np.cumsum(self.reactions[reaction.rev_reaxstr(rxn)][1]),
                            atol=corr_atol, rtol=corr_rtol, nseg=nseg 
                        )

                        # critical is, the system is not in equilibrium, but forward == backward
                        if has_corr:
                            reaction_info[rxn].update({
                                'corr': True,
                                'info': 'Strong correlation with reverse reaction',
                                'back_ratio': back_ratio
                            })

                            if back_ratio < 1.0:
                                scale *= (1.0 - back_ratio)
                            else:
                                continue 

            # only use the last segment
            if ignore_front:
                turnpoint = _select_last_turning_point(
                    cu_conc_product, 
                    cu_rate, 
                    autoslice_atol, 
                    autoslice_rtol,
                    nseg
                    )
                if turnpoint > 0.85 * len(cu_conc_product):
                    reaction_info[rxn]['info'] = 'Too few useful points'

                if slice_ is not None and slice_.start is not None:
                    myslice = slice(max(slice_.start, turnpoint), None, None)
                else:
                    myslice = slice(turnpoint, None, None)
            else:
                myslice = slice_ 

            if myslice is not None and (myslice.start != 0 or myslice.stop != len(rate)):
                cu_rate_sliced = np.cumsum(rate[myslice])
                cu_conc_product_sliced = np.cumsum(conc_product[myslice])
                reaction_info[rxn].update({
                    'sample_begin': (myslice.start if myslice.start else 0) * self.timestep,
                    'sample_end': (myslice.stop if myslice.stop else len(rate)) * self.timestep
                })
            else:
                cu_conc_product_sliced = cu_conc_product
                cu_rate_sliced = cu_rate

            reaction_info[rxn].update({
                'rate_sum_used': np.sum(rate[myslice]),
                'conc_sum_used': np.sum(conc_product[myslice])
            })

            # recheck
            ret, info = self._check_reaction(rate[myslice], conc_product[myslice], reactants, 
                stat_cutoff=stat_cutoff,
                order_cutoff=order_cutoff,
                sample_cutoff=sample_cutoff,
                sample_corr_cutoff=sample_corr_cutoff)
            if ret == False:
                reaction_info[rxn].update({
                    'select': False,
                    'info': info 
                })
                continue

            k, res = _rate_constant(cu_rate_sliced, cu_conc_product_sliced, method=method)

            reaction_info[rxn].update({
                'k': k * volume**(len(reactants) - 1) / self.timestep * scale,
                'res': res,
                'sample_flux': self.timestep * cu_conc_product_sliced[-1]
            })

        # build reaxmap
        return self._build_reaxmap(reaction_info, full=full, keep_removed=keep_removed)

    def _build_reaxmap(self, reaction_info, full=False, keep_removed=False):

        rmap = reaxmap.ReaxMap(species=reaction.species_set(reaction_info.keys()))
        for rxn in reaction_info:
            if reaction.is_canonical(rxn):
                rev_rxn = reaction.rev_reaxstr(rxn)
                kp = reaction_info[rxn]['k'] if reaction_info[rxn]['select'] else 0
                kc = reaction_info[rev_rxn]['k'] if reaction_info[rev_rxn]['select'] else 0

                if kp == 0 and kc == 0 and not keep_removed:
                    continue
                else:
                    rmap.add_reaction(rxn, (kp, kc))

        if full:
            return rmap, reaction_info
        else:
            return rmap 

    def _check_reaction(self, rate, conc_product, reactants, stat_cutoff, order_cutoff, sample_cutoff, sample_corr_cutoff):
        
        if len(reactants) >= order_cutoff:
            return False, 'Too many reactants'

        if np.sum(rate) < stat_cutoff:
            return False, 'Too few reaction samples'

        if np.sum(conc_product) < sample_cutoff**len(reactants):
            if sample_corr_cutoff is None or np.sum(rate) < sample_corr_cutoff:
                return False, 'Too few reactant samples' 

        return True, ''

    def _is_strong_correlation(self, cu_rate_forward, cu_rate_backward, cu_conc_product_forward, cu_conc_product_backward,
        atol, rtol, nseg):

        if not _is_proportional(cu_conc_product_forward, cu_conc_product_backward, atol, rtol, nseg
                ) and _is_proportional(cu_rate_forward, cu_rate_backward, atol, rtol, nseg
                ):
            return True, cu_rate_backward[-1]/cu_rate_forward[-1]
        else:
            return False, 0.0

