
from . import dump

def sample_dump_files(filename_c, filename_r, filename_out, sample_int, sample_range, volume=1.0, combine_reverse=True):
    """ Sample from files (concentration dump, reaction dump)
    """
    if not sample_range:
        sample_range = sample_int
    verbose = False if not filename_out else True 

    rawdata = dump.read_file(filename_c, filename_r, verbose=verbose)
    rsample = rawdata.sample(sample_int, sample_range, volume)
    if combine_reverse:
        rsample = rsample.net_rate_sample()
    

    rsample.write_file(filename_out, verbose=verbose)


def _main(argv):

    import getopt 

    combine_reverse = True
    output = None
    sample_int = 1
    sample_range = None
    volume = 1.0

    long_help_str = '''
Usage: mapcut sample [-i sample_interval] (-r sample_range) [-o output] (-v volume) (--rev) filename_c filename_r

Arguments:
sample_interval: Frame number between sampling;
sample_range: Number of sample points count around central point;
output: Output filename (default is stdout)
volume: Divide volume for each concentration and rate data;
rev: Treat reversible reaction as two seperated ones.

FILE_C, FILE_R:
Concentration dump filename and rate dump filename.
    '''

    try:
        opts, args = getopt.getopt(argv[1:], 'hi:o:r:v:', ['rev', 'help'])
    except getopt.GetoptError:
        print('Invalid argument. Use `-h` see helps')
        return 1

    for opt, arg in opts:
        if opt == '-h' or opt == '--help':
            print(long_help_str)
            return 1
        elif opt == '-i':
            sample_int = int(arg)
        elif opt == '-o':
            output = arg
        elif opt == '-r':
            sample_range = int(arg)
        elif opt == '-v':
            volume = float(arg)

        elif opt == '--rev':
            combine_reverse = False

    assert len(args) == 2, 'No enough dump file and reaction file'

    sample_dump_files(args[0], args[1], output, sample_int, sample_range, volume, combine_reverse)
    return 0