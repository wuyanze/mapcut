""" Handles merge of reaxsample
"""
import getopt
import numpy as np 
from ..core import reaxmap, reaxsample, ReaxMap, ReaxSample

def merge_reaxmap(rmaps):
    """ Merge reaxmaps. Kinetic constant will be averaged.
    Args:
    ---
    reaxmaps: list of reaxmaps.
    
    Returns:
    ---
    ReaxMap instance.
    """
    target_species = set()
    target_reactions = set()
    for rmap in rmaps:
        target_species.update(rmap.CheckSpecies())
        target_reactions.update(rmap.CheckReactions())

    mymap = ReaxMap(species=target_species)
    for r in target_reactions:
        klist = []
        for rmap in rmaps:
            try:
                klist.append(rmap.index_reaction(r).k)
            except KeyError:
                pass 
        klist = np.array(klist)
        mymap.add_reaction(r, (np.sum(klist, axis=0)/len(rmaps)).tolist())

    return mymap


def merge_reaxsample(reaxsamples, option='union'):
    """ Merge reaxsamples. option can be 'union', 'intersection'
    Args:
    ---
    reaxsamples: list of reaxsamples with same sample points and initial state.
        (can have different species and reactions)
    option: 'union' -> output union of samples (both reactions and species);
            'intersection' -> output intersection of samples (both reactions and species);

    Returns:
    ---
    ReaxSample instance, with rate and concentration averaged.
    """
    target_species = set()
    target_reactions = set()

    # check timestamp
    for sample in reaxsamples[1:]:
        assert sample.c.index == reaxsamples[0].c.index, "Time stamp not match!"
        assert sample.init == reaxsamples[0].init, 'Initial concentration not match!'
        assert sample.include_reverse == reaxsamples[0].include_reverse

    if option == 'union':
        for sample in reaxsamples:
            target_species.update(sample.c.columns)
            target_reactions.update(sample.r.columns)

    elif option == 'intersection':
        target_species = set(reaxsamples[0].c.columns)
        target_reactions = set(reaxsamples[0].r.columns)

        for sample in reaxsamples[1:]:
            target_species.intersection_update(sample.c.columns)
            target_reactions.intersection_update(sample.r.columns)

    else:
        raise ValueError(option)

    data_species = dict.fromkeys(target_species, np.zeros(len(reaxsamples[0])))
    data_reactions = dict.fromkeys(target_reactions, np.zeros(len(reaxsamples[0])))

    for sample in reaxsamples:
        for s in data_species:
            data_species[s] += sample[s]

        for r in data_reactions:
            data_reactions[r] += sample.rate(r)

    # average
    for s in data_species:
        data_species[s] = data_species[s]/len(reaxsamples)
    for r in data_reactions:
        data_reactions[r] = data_reactions[r]/len(reaxsamples)

    dst_sample = ReaxSample(
        c=DataFrame(data_species, index=reaxsamples[0].c.index),
        r=DataFrame(data_reactions, index=reaxsamples[0].r.index),
        init=reaxsamples[0].init
    )
    return dst_sample


def _main(argv):
    """ Main entry for merge command.
    """
    opts, args = getopt.getopt(argv[1:], 'mso:h', ['help'])

    help_str = '''
Usage: mapcut merge [-s/-m] [-o output] FILE...

Arguments:
-m: Specify ReaxMap as merge object;
-s: Specify ReaxSample as merge object;
-o: Output name, default is `stdout`.

FILE:
Input filenames depending on task type.'''

    task = None
    output = None 

    for opt, arg in opts:
        if opt == '-m':
            task = 'map'
        elif opt == '-s':
            task = 'sample'
        elif opt == '-o':
            output = arg
        elif opt == '-h' or opt == '--help':
            print(help_str)
            return 
        
    assert args, 'Error: No input file'

    if task == 'map':
        rmaps = [reaxmap.read_file(r) for r in args]
        merge_reaxmap(rmaps).write_file(output)
    elif task == 'sample':
        samples = [reaxsample.read_file(r) for r in args]
        merge_reaxsample(samples[0], samples[1:]).write_file(output)
    else:
        raise ValueError(task)
