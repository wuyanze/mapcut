""" Analyze rate constant.
"""

import getopt 
import os.path 

from ..util import parse 
from . import dump
from .ratelaw import ReaxStat


def write_full_output(filename, reaction_info):

    with open(filename, 'w') as f:
        f.write('NAME\tSELECT\tK\tRES\tBEGIN\tEND\tFLUX\tSUMR\tSUMRU\tSUMC\tSUMCU\tINFO\n')

        for rxn, data in sorted(reaction_info.items(), key=lambda x:(not x[1]['select'], x[0])):
            name = str(rxn)
            f.write('%s\t%s\t%.6g\t%.6g\t%.4g\t%.4g\t%.4g\t%d\t%d\t%d\t%d\t%s\n' % (
                rxn,
                'TRUE' if data['select'] else 'FALSE',
                data['k'],
                data['res'],
                data['sample_begin'],
                data['sample_end'],
                data['sample_flux'],
                data['rate_sum_tot'],
                data['rate_sum_used'],
                data['conc_sum_tot'],
                data['conc_sum_used'],
                data['info']
            ))


def _main(argv):

    add = 'para'
    full = True 
    output = None
    file_config = None
    volume = 1.0
    kwargs = {}

    help_str = """
Usage: mapcut rate [-v volume] (-c config) (-o output) (--overlay=) (--simple) (--kwd=value...) [FILE_C], [FILE_R] ...

Arguments:
volume: System volume;
config: Configure file recording volume and time interval;
output: Output filename (default is stdout);
overlay: Overlay style of samples, can be 'para' or 'seq';
simple: Do not output report file;

Keyword Arguments:
Additional arguments will be passed to ReaxStat.to_reaxmap().

FILE_C, FILE_R:
Must be name of concentration dump and reaction rate dump files.
"""

    try:
        opts, args = getopt.getopt(argv[1:], 'c:v:o:h', [
            'overlay=', 'help', 'simple', 
            'stat_cutoff=', 'order_cutoff=', 'sample_cutoff=', 'sample_corr_cutoff=', 'corr_detect=',
            'ignore_front=', 'keep_removed=', 'slice_=', 'nseg=', 'autoslice_atol=', 'autoslice_rtol=',
            'corr_atol=', 'corr_rtol='
            ])
    except getopt.GetoptError:
        print('Invalid argument. Use `-h` see helps')
        return 1

    for opt, arg in opts:
        if opt == '-v':
            volume = float(arg)
        elif opt == '-o':
            output = arg
        elif opt == '-c':
            file_config = arg
        elif opt == '--overlay':
            add = arg 
        elif opt == '--simple':
            full = False 
        elif opt == '-h' or opt == '--help':
            print(help_str)
            return 1
        else:
            kwargs[opt[2:]] = parse.parse_token(arg)

    # read config from file
    if file_config:
        with open(file_config, 'r') as f_config:
            configs = parse.parse_dict([line.strip('\n').strip() for line in f_config.readlines()])
            volume = configs['volume']
            timestep = configs['interval']

    nrows = kwargs['slice_'].stop if 'slice_' in kwargs else None

    assert len(args) >= 2 and len(args) % 2 == 0, 'Please input raw data file and raw rate file'

    rstat = ReaxStat()

    for i in range(0, len(args), 2):
        rawdata = dump.read_file(args[i], args[i+1], nrows=nrows)
        rstat.load(rawdata, add=add)

    rstat.submit()
    rmap, reaction_info = rstat.to_reaxmap(volume=volume, full=True, **kwargs)

    rmap.write_file(output)

    if full:
        if output:
            fulloutput_name = output + '.report'
        else:
            fulloutput_name = 'mapcut.rate.report'
    
        write_full_output(fulloutput_name, reaction_info)
