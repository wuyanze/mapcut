""" Handles the merge and difference tasks.
"""

import csv
import sys
import getopt
import numpy as np
from pandas import DataFrame

from ..core import reaxsample
from ..core import reaxmap
from . import dump
from ..util import reaction

class ReaxMapDiff(object):
    """ Describe the difference of reaxmaps
    """
    def __init__(self, species_freq=None, reactions=None):
        self.species_freq = species_freq
        self.reactions = reactions
        self.count = 0

    def diff_reaxmap(self, reaxmaps):
        """ Load several maps, and put the common species and reactions out.
        """
        self.count = len(reaxmaps)
        self.species_freq = dict()
        for idx, reaxmap in enumerate(reaxmaps):
            for s in reaxmap.CheckSpecies():
                if not s in self.species_freq:
                    self.species_freq[s] = [0]*len(reaxmaps)
                self.species_freq[s][idx] += 1

        reaction_names = set()
        for reaxmap in reaxmaps:
            reaction_names.update(reaxmap.CheckReactions())

        self.reactions = dict.fromkeys(reaction_names)
        for r in self.reactions:
            self.reactions[r] = {
                'kp':[0]*len(reaxmaps),
                'km':[0]*len(reaxmaps),
                'count':0,
                }

        for idx, reaxmap in enumerate(reaxmaps):
            for reaction in reaxmap.prior_reactions():
                name = str(reaction)
                self.reactions[name]['kp'][idx] = reaction.k[0]
                self.reactions[name]['km'][idx] = reaction.k[1]
                self.reactions[name]['count'] += 1

    def to_csv(self, output, titles=None, sep=','):
        """ Write formatted output from reaxmap different results.
        """
        if not titles:
            titles = ['reaxmap%d' % i for i in range(self.count)]

        f = open(output, 'w') if output else sys.stdout

        writer = csv.writer(f, delimiter=sep)

        writer.writerow(['Species Diff'])
        writer.writerow(['name', 'count'] + titles)
        for name, freq in sorted(self.species_freq.items(), key=lambda x:(-sum(x[1]), x[0])):
            writer.writerow([name, sum(freq)] + list(map(str, freq)))
        writer.writerow('')
        writer.writerow(['Reactions Diff'])
        writer.writerow(['name', 'count'] + ['%s_pro' % t for t in titles] + ['name_rev'] + ['%s_con' % t for t in titles])
        for name, freq in sorted(self.reactions.items(), key=lambda x:(-x[1]['count'], x[0])):
            writer.writerow([name, freq['count']] + list(map(str, freq['kp'])) + [reaction.rev_reaxstr(name)] + list(map(str, freq['km'])))

        if f is not sys.stdout:
            f.close() 


class ReaxSampleDiff(object):
    """ Describe the difference between reaxsamples.
    """
    def __init__(self, species_errors=None):
        self.species_errors = species_errors
        self.count = 0

    def diff_reaxsample(self, sample_base, samples_compare):
        """ Calculate the difference between samples with sample_base
        Args:
            sample_base: ReaxSample instance;
            samples_compare: list of ReaxSample instances;
        """
        assert samples_compare, 'No samples to compare!'

        self.species_errors = dict.fromkeys(sample_base.species.keys())
        self.count = len(samples_compare)
        for s in self.species_errors:
            self.species_errors[s] = {'name':s}

        for sample in samples_compare:
            comparison = sample.compare_with(sample_base)
            for s in self.species_errors:
                if s in comparison['error']:
                    self.species_errors[s][filename_in] = str(comparison['error'][s]['re_int'])
                else:
                    self.species_errors[s][filename_in] = 'N/A'

    def to_csv(self, output, titles=None, sep=None):

        if not titles:
            titles = range(len(self.count))

        f = open(output, 'w') if output else sys.stdout

        writer = csv.DictWriter(f, ['name'] + titles, delimiter=sep)
        writer.writeheader()
        writer.writerows(self.species_errors.values())

        if f is not sys.stdout:
            f.close()


def diff_reaxmap(reaxmaps):
    """ See the difference of reaxmaps.
    Args:
        reaxmaps: list of ReaxMap objects
    Returns:
        map_diff: ReaxMapDiff object
    """
    map_diff = ReaxMapDiff()
    map_diff.diff_reaxmap(reaxmaps)
    return map_diff


def diff_reaxsample(sample_base, samples_compare):
    """ Calculate the difference between samples with sample_base
    Args:
        sample_base: ReaxSample instance;
        samples_compare: list of ReaxSample instances;
    Returns:
        ReaxSampleDiff instance
    """

    sample_diff = ReaxSampleDiff()
    sample_diff.diff_reaxsample(sample_base, samples_compare)
    return sample_diff


def _main(argv):
    """ Main entry for diff method.
    """
    opts, args = getopt.getopt(argv[1:], 'mso:h', ['help'])
    help_str = '''
Usage: mapcut diff [-m/-s] (-o output) [FILE]...

Arguments:
-m/-s: Specify task type (reaxmap or reaxsample);
-o: Output filename (default is `stdout`).

FILE: Input filenames depending on task type.
'''

    task = None
    output = None

    for opt, arg in opts:
        if opt == '-m':
            task = 'map'
        elif opt == '-s':
            task = 'sample'
        elif opt == '-o':
            output = arg
        elif opt == '-h':
            print(help_str)
            return 

    assert args, 'Error: no input'

    if task == 'map':
        maps = [reaxmap.read_file(r) for r in args]
        diff_reaxmap(maps).to_csv(output, titles=args)
    elif task == 'sample':
        assert len(args) > 1, 'Error: no enough input'
        samples = [reaxsample.read_file(r) for r in args]
        diff_reaxsample(samples[0], samples[1:]).to_csv(output)
