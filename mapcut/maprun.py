
import sys
import numpy as np
import pandas
from scipy.integrate import odeint

from .stats import ReaxRawData
from .util import errors
from .util import numeric
from .util import reaction
from .util.parse import parse_dict


class MapRunner:
    """ Evaluating reaction system (a `ReaxMap` instance) via solving differential equations.

    Example:

    ```
    > from mapcut import ReaxMap, MapRunner
    > rmap = ReaxMap(['a', 'b', 'c'])
    > rmap.add_reaction('a+b=c', k=(1.0, 0.1))
    > runner = MapRunner()
    > runner.load(rmap)
    > runner.set_c0({'a':10.0, 'b':5.0})    # set initial concentration
    > runner.set_dump('dump.csv')   # set dump file
    > runner.run(time=10.0, timestep=1.0)
    Start mapcut running session...
    Integral timestep=10.000000
    Integral from 0.000000 - 10.000000
    Running...finished
    > runner.write_restart('restart.txt')   # write restart concentration
    ```
    """

    def __init__(self):
        self.c_init = None
        self.dump_file = None
        self.dump_mask = None 
        self.last_dump_idx = 0
        self.reaxmap = None
        self.results = []
        self.run_args = ()  # tuple: Reactant matrix, product matrix, kp array, kn array
        self.dump_args = () # tuple: Interval, cutoff
        self.time_offset = 0.0 # used in restart session

    def __repr__(self):
        return '(Maprunner with %d species, init=%r, last dump=%d)' % (len(self.reaxmap.species), self.c_init, self.last_dump_idx)

    def clear(self):
        """ Clear data for new running session.
        """
        self.c_init = None
        self.dump_mask = None
        self.last_dump_idx = 0
        self.reaxmap = None
        self.results.clear()
        self.run_args = ()
        self.timestep = None
        self.time_offset = 0.0 

    def load(self, reaxmap):
        """ Load a reaxmap. The order of reaction and species are exactly same
        as reaxmap. Will **NOT** include species with importance less than `Priority.NORMAL`.

        Args:
            reaxmap: `ReaxMap` instance.
        """
        self.reaxmap = reaxmap.skeleton()
        list_r, list_p, kp, kn = self.reaxmap.toarray(kinetics=True)
        self.run_args = (numeric.List2Mat(list_r, len(self.reaxmap.species)).toarray(), # matrix of reactants
                        numeric.List2Mat(list_p, len(self.reaxmap.species)).toarray(),  # matrix of products
                        kp, 
                        kn)
            
    def set_c0(self, c0_dict, strict=True):
        """ Set initial concentration of species.

        Args:
            c0_dict: `dict` of speciesname: concentration (`float`);
            strict: raise error when a species in `c0_dict` not found in system.
        """
        self.c_init = np.linspace(0.0, 0.0, len(self.reaxmap.species))
        species_list = self.reaxmap.CheckSpecies()
        for name, conc in c0_dict.items():
            try:
                self.c_init[species_list.index(name)] = conc
            except ValueError:
                if strict:
                    raise errors.NotExist(name, 'set_c0')

    def set_resume(self):
        """ Continue last running.
        """
        if not self.results:
            raise RuntimeError('This is the first time running, cannot continue')
        else:
            self.c_init = self.results[-1][0]
            self.results.pop()

    def set_dump(self, filename, interval=1, species=None, cutoff=1e-10, append=False, filename_r=None, **kwargs):
        """ Setting dump arguments.

        Args:
            filename: filename to write;
            interval: `int`, the relative interval in results;
            species: species list for dump (Set `None` to dump all species);
            cutoff: limitation of concentration;
            append: whether write file for append;
            filename_r: filename of reaction dump.
        """
        if species is None:
            self.dump_mask = np.array([True] * len(self.reaxmap.species))
        else:
            self.dump_mask = np.array([False] * len(self.reaxmap.species))
            for i, name in enumerate(self.reaxmap.species):
                self.dump_mask[i] = name in species

        self.dump_args = (interval, cutoff,)
        dump_mode = 'a' if append else 'w'
        
        # concentration must be dump, reaction is optional
        self.dump_file = open(filename, dump_mode)
        self.dump_file_r = open(filename_r, dump_mode) if filename_r else None

        if append:
            return

        self.dump_file.write('t,%s\n' % (','.join(self.reaxmap.CheckSpecies())))
        if self.dump_file_r:
            reaction_names_with_rev = self.reaxmap.CheckReactions() + [
                reaction.rev_reaxstr(rxn) for rxn in self.reaxmap.CheckReactions()]
            self.dump_file_r.write('t,%s\n' % (','.join(reaction_names_with_rev)))

    def set_actual_timestep(self, size_space, size_time):
        """ Set the proper timestep for intergral. Do not use arbitarily.
        """
        return min(max(1, int(80/(size_space/1000)**2)) * self.timestep, size_time)

    def run(self, time, timestep, **kwargs):
        """ Top interface for running results.

        Args:
            time: float, total running time;
            timestep: float, running interval for reference (not actual interval).

        Kwargs:
            auto_dump: Dump the output while running, default is `True`;
            int_timestep: Specify timestep for integration instead of automatically determining;
            verbose: Display message while running, default is `True`;
            
        Other kwargs will be passed to `_run_system()`.
        """
        # setting running arguments
        assert self.run_args, 'Cannot run without initialize!'
        mat_r, mat_p, kp, kn = self.run_args

        # setting dump arguments
        if not self.dump_file:
            print('Warning: will not dump results', file=sys.stderr)
            self.dump_file_r = None
        else:
            dump_interval, dump_cutoff = self.dump_args

        # setting initial data 
        self.timestep = timestep
        c0 = self.c_init
        int_timestep = kwargs.get('int_timestep', self.set_actual_timestep(len(kp), time))
        verbose = kwargs.get('verbose', True)
        auto_dump = kwargs.get('auto_dump', True)

        if verbose:
            print('Start mapcut running session...')
            print('Integral timestep=%f' % int_timestep)

        for t in np.arange(0.0, time, int_timestep):
            if verbose:
                print('Integral from %f - %f' % (t, min(time, int_timestep+t)))
            c, rf, rb, success = _run_system(mat_r, mat_p, kp, kn, c0, min(int_timestep, time-t), timestep, **kwargs)
            if verbose and not success:
                print('Warning: Integration not succeeded!')

            for c_, rf_, rb_ in zip(c[:-1], rf[:-1], rb[:-1]):
                self.results.append((c_, rf_, rb_))

            c0 = c[-1]
            if auto_dump and self.dump_file:
                self.dump(dump_interval, dump_cutoff)

        self.results.append((c[-1], rf[-1], rb[-1]))

        if self.dump_file:
            self.dump_file.close()
        if self.dump_file_r:
            self.dump_file_r.close()

        if verbose:
            print('Running...finished')

    def get_result(self, trim_last=False):
        """ Return result as `ReaxRawData` instance.

        Args:
            trim_last: Remove the result at the last time point.
        """
        rawdata = ReaxRawData()
        t = np.arange(self.time_offset, self.time_offset + self.timestep * len(self.results), self.timestep).tolist()
        
        _results = self.results if not trim_last else self.results[:-1]
        _t = t if not trim_last else t[:-1]

        rawdata.c_dump = pandas.DataFrame(
            [result[0] for result in _results], 
            index=_t, 
            columns=self.reaxmap.CheckSpecies())

        reaction_names_with_rev = self.reaxmap.CheckReactions() + [
            reaction.rev_reaxstr(rxn) for rxn in self.reaxmap.CheckReactions()]

        rawdata.r_dump = pandas.DataFrame(
            [np.concatenate(result[1:3]) for result in _results], 
            index=_t,
            columns=reaction_names_with_rev)
        return rawdata

    def dump(self, interval, cutoff=1e-10, sep=','):
        """ Write calculation results to file. Only writting the updated part (the data that has written before is not written again).
        This method is automatically called in `run()` if `auto_dump` is set to `True`.

        Args:
            interval: `int`, step of dump;
            cutoff: value less than cutoff will be set to 0;
            sep: separator of result, default is `,`.
        """
        if not self.dump_file:
            return
        last_dump_idx_r = self.last_dump_idx
        while self.last_dump_idx < len(self.results):
            row = numeric.MatFilter(self.results[self.last_dump_idx][0][self.dump_mask], cutoff)
            self.dump_file.write('%.5g,%s\n' % (self.last_dump_idx * self.timestep + self.time_offset, ','.join(map(str, row))))
            self.last_dump_idx += interval
        self.dump_file.flush()

        if not self.dump_file_r:
            return
        while last_dump_idx_r < len(self.results):
            row = numeric.MatFilter(np.concatenate(self.results[last_dump_idx_r][1:3]), cutoff)
            self.dump_file_r.write('%.5g,%s\n' % (last_dump_idx_r * self.timestep + self.time_offset, ','.join(map(str, row))))
            last_dump_idx_r += interval
        self.dump_file_r.flush()

    def read_restart(self, filename):
        """ Read restart file.
        """
        with open(filename, 'r') as file_rst:
            lines = [line.strip('\n').strip() for line in file_rst]
            self.time_offset = float(lines[0][2:]) # 't=xxx'
            begin_conc = parse_dict(lines[1:])
            self.set_c0(begin_conc)

    def write_restart(self, filename):
        """ Write restart file.
        """
        with open(filename, 'w') as file_rst:
            file_rst.write('t=%.5g\n' % (self.time_offset + self.last_dump_idx * self.timestep))
            for name, c in zip(self.reaxmap.CheckSpecies(), self.results[-1][0]):
                file_rst.write('%s=%.10e\n' % (name, c))


def _run_system(mat_r, mat_p, kp, kn, c0, time, interval, **kwargs):
    """ Running a reaction system by solving ODEs.

    Arguments:
        mat_r, mat_p: np.array, reaction matrix of reactants and products.
        kp, kn: np.array, forward and backward rate constants
        c0: initial rate;
        time: integrate time;
        interval: output interval;
        atol: tolerance;

    Returns:
        c: concentration at different times (include c0);
        rf: forward reaction rate at different times;
        rb: backward reaction rate at different times;
        success: True/False
    """

    atol = kwargs.get('atol', np.max(c0)*1e-4)

    mat_reaction = np.concatenate((mat_r, mat_p)) # a single large matrix containing two directions of reaction
    _mat_delta = (mat_p - mat_r)
    mat_delta = np.transpose(np.concatenate((_mat_delta, -_mat_delta)))
    k = np.concatenate((kp, kn))
    rsize = len(k)
    mat_bool = mat_reaction > 0
    mat_reaction = mat_reaction * mat_bool

    mat_less_than_i = [(mat_reaction <= i)*1 for i in range(1, np.max(mat_reaction))]
    mat_more_than_i = [(mat_reaction > i) for i in range(1, np.max(mat_reaction))]

    def React(c, t):
        return mat_delta.dot(Rate(c))


    def Rate(c):
        powered_c = mat_reaction * c
        for mmti, mlti in zip(mat_more_than_i, mat_less_than_i):
            powered_c *= (mmti*c + mlti)

        return list(map(lambda i: np.prod(powered_c[i, mat_bool[i]]), range(rsize))) * k


    def RealRate(c):
        r = Rate(c)
        return r[0:rsize//2] - r[rsize//2:rsize]

    def RealRate_forward(c):
        return Rate(c)[0:rsize//2]

    def RealRate_backward(c):
        return Rate(c)[rsize//2:]

    c, info = odeint(React, c0, np.arange(0, time + interval, interval), full_output=True, atol=atol)
    
    if kwargs.get('net', False):
        return c, np.apply_along_axis(RealRate, arr=c, axis=1) # net reaction rate
    else:
        return (c, 
            np.apply_along_axis(RealRate_forward, arr=c, axis=1), 
            np.apply_along_axis(RealRate_backward, arr=c, axis=1), 
            'successful' in info['message'])
