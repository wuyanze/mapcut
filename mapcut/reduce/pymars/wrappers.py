""" Wrappers to convert data structure into pyMARS-recognizable forms.
"""

from collections import namedtuple
import numpy as np 

from ..share import GetReactionMatrix

SpeciesObject = namedtuple('SpeciesObject', ['name'])

class SolutionObject:
    """ Equivalence to ct.colution object
    """
    
    def __init__(self, mapNumArray):
        
        ipt_s, ipt_r, list_r, list_p = mapNumArray

        self._species = [SpeciesObject(str(i)) for i in range(len(ipt_s))] # species name is actually '1', '2', ...

    def species(self):
        return self._species

    def reactions(self):
        return None 



def get_rate_data_drg(mapNumArray, rsample):
    """ Return an edge data dict, containing drg data index.

    total_edge_data
        - edge_data_ic
            - edge_data_step
                - 0
                - 1
                    - species:denominator
                - 2
                    - edge: numerator
    """
        
    ipt_s, ipt_r, list_r, list_p = mapNumArray
    c, rate = rsample
    size = len(ipt_s)

    mat_reac_abs = np.abs(GetReactionMatrix(list_r, list_p, size, rate))    # |\nu_{i,j}*r_i
    mat_reac_bool = (mat_reac_abs > 0) * 1  # \delta_j^i
    
    edges = {}
    for i in range(size):
        for j in range(size):       
            if i != j:     
                edges['%d_%d' % (i, j)] = np.sum(np.multiply(mat_reac_abs[:, i], mat_reac_bool[:, j]))

    nodes = {}

    for j in range(size):
        nodes[str(j)] = np.sum(mat_reac_abs[:, j])

    total_edge_data = {0:{0:(
        None, nodes, edges
    )}}

    return total_edge_data


def get_rate_data_drgep(mapNumArray, rsample):

    ipt_s, ipt_r, list_r, list_p = mapNumArray
    c, rate = rsample
    size = len(ipt_s)

    mat_reac = GetReactionMatrix(list_r, list_p, size, rate)    # |\nu_{i,j}*r_i
    mat_reac_bool = (mat_reac != 0) * 1  # \delta_j^i
    
    edges = {}
    for i in range(size):
        for j in range(size):
            if i != j:        
                edges['%d_%d' % (i, j)] = abs(np.sum(np.multiply(mat_reac[:, i], mat_reac_bool[:, j])))

    nodes = {}

    for j in range(size):
        l = mat_reac[:, j]
        nodes[str(j)] = max(np.sum(l[l > 0]), -np.sum(l[l < 0]))

    total_edge_data = {0:{0:(
        None, nodes, edges
    )}}

    return total_edge_data
