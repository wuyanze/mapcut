import numpy as np
import scipy.sparse as sps

from ..util import numeric


def GetReactionMatrix(list_r, list_p, size, rate):
    """ Return a reaction matrix where r[i][j] = rate_i*coe_j;
    """
    mat_reaction = numeric.List2Mat(list_p, size) - numeric.List2Mat(list_r, size)
    return np.dot(sps.diags(rate), mat_reaction).toarray()


def Map2Graph(reactants_list, products_list, weight, size, *options):
    """ Turn reaction matrix to adjacent matrix.
    Args:
        reactants_list: numpy array of reactants in reactions;
        products_list: numpy array of products in reactions;
        weight: reaction rates list, numpy array;
        size: number of species, int;
        options: can be 'abs', 'net', 'cos':
            'abs' means we doesn't care whether it is product or reactant in a reaction;
            'net' means we care it;
            'cos' means we calculate both two sides, rather than the flow;
            Specifically:
            'net': r[i][j] = sum(rate_k*-min(coe_(k,i),0)*max(coe_(k,j),0))
            'cos', 'net': r[i][j] = sum(rate_k*coe_(k,i)*coe_(k,j));
            'cos', 'abs': r[i][j] = sum(|rate_k*coe_(k,i)*coe_(k,j)|);
    Return:
        scipy.sparse matrix.
    """
    for option in options:
        if option not in ['net', 'abs', 'cos']:
            raise ValueError(option)

    mat_r = numeric.List2Mat(reactants_list, size)
    mat_p = numeric.List2Mat(products_list, size)

    if 'abs' in options:
        mat_W = sps.diags(np.abs(weight))
    else:
        mat_W = sps.diags(weight)
    
    if 'cos' in options:
        mat_coeff = mat_p - mat_r
        mat_coeff_bool = (np.abs(mat_coeff) > 0) * 1 # set the value to 1 to avoid repeat

        if 'abs' in options:
            mat_coeff = np.abs(mat_coeff)
        return np.dot(np.transpose(mat_coeff), np.dot(mat_W, mat_coeff_bool))

    else:
        raw_graph = np.dot(np.transpose(mat_r), np.dot(mat_W, mat_p))

        if 'net' in options:
            return raw_graph - raw_graph.transpose(copy=True)
        elif 'abs' in options:
            return raw_graph + raw_graph.transpose(copy=True)


def Restore_species(remain_s, ipt_s, ipt_r, list_r, list_p):
    """ Restore the reaxmap array by remain species.
        Only reactions between species in remain_s will be kept.
    Return:
        Tuple which can be decoded into ReaxMap.
    """
    new_ipt_s = numeric.MatFilter(ipt_s, 8)

    for rs_idx in remain_s:
        new_ipt_s[rs_idx] = max(new_ipt_s[rs_idx], 4)

    new_ipt_r = np.copy(ipt_r)
    for idx in range(len(list_r)):
        for reactant in list_r[idx]:
            if not reactant in remain_s:
                new_ipt_r[idx] = 0
                break
        for product in list_p[idx]:
            if not product in remain_s:
                new_ipt_r[idx] = 0
                break
    return (new_ipt_s, new_ipt_r, list_r, list_p)


def Restore_reaction(remain_r, ipt_s, ipt_r, list_r, list_p):
    """ Restore the reaxmap array by remain reactions.
    """
    new_ipt_r = np.zeros(len(list_r))
    for idx in remain_r:
        new_ipt_r[idx] = 4

    new_ipt_s = numeric.MatFilter(ipt_s, 8)
    for idx in remain_r:
        for reactant in list_r[idx]:
            new_ipt_s[reactant] = max(new_ipt_s[reactant], 4)
        for product in list_p[idx]:
            new_ipt_s[product] = max(new_ipt_s[product], 4)
    return new_ipt_s, new_ipt_r, list_r, list_p


def Restore(restore_list, option, mapNumArray):
    """ Higher interface for restoring.
    Args:
        restore_list: Species or reaction index list for restore;
        Notice: restore_list must not include ignored species/reactions;
        option: 'species' or 'reaction';
    """
    if option == 'species':
        return Restore_species(restore_list, *mapNumArray)
    elif option == 'reaction':
        return Restore_reaction(restore_list, *mapNumArray)

