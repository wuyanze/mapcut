
import copy 
import numpy as np 
import itertools

from .core import reaxmap, reaxsample
from .maprun import MapRunner
from .reduce import reduce_map


class Series:
    """ A time squence of reaxmaps.
    """

    def __init__(self, rmaps=None, intervals=None):
        self.rmaps = rmaps if rmaps else []
        self.intervals = intervals if intervals else [] 

        assert len(rmaps) == len(intervals), 'Number of time not match number of reaxmap'

        self._check_consistency()

    def _check_consistency(self):
        
        set_species = set(itertools.chain(*[rmap.CheckSpecies() for rmap in self.rmaps]))
        set_reactions = set(itertools.chain(*[rmap.CheckReactions() for rmap in self.rmaps]))

        for rmap in self.rmaps:
            for s in set_species.difference(rmap.CheckSpecies()):
                rmap.add_species(s)
            for r in set_reactions.difference(rmap.CheckReactions()):
                rmap.add_reaction(r)
            rmap.sort()

    def append(self, rmap, interval):
        """ Add one `ReaxMap` object.

        Args:
            rmap: `ReaxMap` object.
            interval: `float`, time that the `ReaxMap` object holds.
        """

        self.rmaps.append(rmap)
        self.intervals.append(interval)

    def CheckSpecies(self, option):
        return self.rmaps[0].CheckSpecies(option)

    def set_priority(self, species, priority=reaxmap.Priority.CRITICAL):
        """ Set priority species for all `ReaxMap` objects inside. See `ReaxMap.set_priority()`
        for details.

        Args:
            species: `str` or list of `str`, name of species.
            priority: `int`.
        """
        for rmap in self.rmaps:
            rmap.set_priority(species, priority)

    def toreaxmaplist(self):
        """ Get the reaction maps included.

        Returns:
            list of `ReaxMap` objects.
        """
        return [r.skeleton(True) for r in self.rmaps]

    def run_all(self, timestep, c0_dict, cdump, rdump, dumpinterval=1, **kwargs):
        """ Run kinetic simulation based on reaction maps contained. Every reaction map
        will be applied sequentially, based on their time range recorded. See `MapRunner.run()`
        for details.

        Args:
            timestep: `float`, simulation timestep.
            c0_dict: `dict` mapping species name and initial concentration.
            cdump: `str`, filename of concentration dump.
            rdump: `str`, filename of rate dump.
            dumpinterval: `int`, number of timesteps between two dumps.

        Additional arguments will be passed to `MapRunner.set_dump()` and `MapRunner.run()`.

        Returns:
            `MapRunner` object.
        """

        assert len(self.rmaps) > 0, 'No data to run'

        runner = MapRunner()            
        runner.load(self.rmaps[0])
        if isinstance(c0_dict, str):
            runner.read_restart(c0_dict)
        else:
            runner.set_c0(c0_dict)
        if cdump is not None:
            runner.set_dump(cdump, interval=dumpinterval, append=False, filename_r=rdump, **kwargs)
        runner.run(self.intervals[0], timestep, **kwargs)

        for rmap, interval in zip(self.rmaps[1:], self.intervals[1:]):

            runner.load(rmap)
            if cdump is not None:
                runner.set_dump(cdump, interval=dumpinterval, append=True, filename_r=rdump, **kwargs)
            runner.set_resume()
            runner.run(interval, timestep, **kwargs)

        return runner

    def reduce_all(self, rsample, method, *args, **kwargs):
        """ Perform reduction on all `ReaxMap` objects. See `reduce_map()` for details.

        Args:
            rsample: `ReaxSample` object, with subsample number _more_ than series size.
            method: `str`, can be "drg", "drgep" or "cspi" by default.

        Additional arguments and keyword arguments will be passed to `reduce_map()`.

        Returns:
            `Series` object containing all reduced reaxmaps.

        Note:
            For `ReaxSample` object with multiple samples, the subsamples are mapped to reaction
            maps contained based on time. It is recommended to use keep the subsample number in
            `ReaxSample` object sample as series size.
        """

        # split reaxsamples
        last_idx = 0
        subsamples = []
        for tp in np.cumsum(self.intervals):
            cur_idx = np.searchsorted(rsample.t() > tp, True)
            subsamples.append(rsample[last_idx:cur_idx])
            last_idx = cur_idx
            if len(subsamples[-1]) == 0:
                raise RuntimeError('Cannot find proper samples for reaxmap %d' % len(subsamples))

        reduce_maps = [reduce_map(self.rmaps[i], subsamples[i], method, *args, **kwargs)[0] for i in range(len(self.rmaps))]

        return Series(reduce_maps, self.intervals)

    
