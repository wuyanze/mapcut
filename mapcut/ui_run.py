
from .util.parse import parse_opt
from .util.iohandle import check_opt

from .core import reaxmap
from .maprun import MapRunner
from .series import Series

def main(argv):

    args, kwargs = parse_opt(argv[1:], {'b':'begin', 't':'time', 's':'timestep', 'o':'dump', 'r':'restart', 'h':'help'}, multiple=False)
    check_opt(args, kwargs, opt_required=['begin', 'timestep', 'dump'], help_document="""
Usage: mapcut run [OPTION] ... [FILE] ...

Compute a reaction system by solving ODEs.

OPTION:
-s, --timestep: Timestep for running.
-b, --begin: Begin concentration or restart file.
-t, --time: Total time for running, if there are one map.
-i, --interval: Interval of each reaxmap that holds within, if there are multiple maps.
-o, --dump: Dump file of concentration.
--rdump: Dump file of reaction rates.
--restart: Restart filename.
--dump_interval: Interval for dump.
-h, --help: Display this help.

Additional options will be passed to MapRunner.run and MapRunner.set_dump.

FILE:
Reaction kinetic data files""")

    begin = kwargs.pop('begin')
    dumpfile = kwargs.pop('dump')
    rdumpfile = kwargs.get('rdump', None)
    if 'rdump' in kwargs:
        kwargs.pop('rdump')
    dumpinterval = kwargs.get('dump_interval', 1)
    if 'dump_interval' in kwargs:
        kwargs.pop('dump_interval')
    timestep = kwargs.pop('timestep')

    rmaps = [reaxmap.read_file(r) for r in args]
    times = kwargs.pop('time')
    if not isinstance(times, list):
        times = [times]

    series = Series(rmaps, times)
    runner = series.run_all(timestep=timestep, c0_dict=begin, cdump=dumpfile, rdump=rdumpfile, dumpinterval=dumpinterval, **kwargs)

    if 'restart' in kwargs:
        runner.write_restart(kwargs['restart'])
