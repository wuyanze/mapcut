""" User interaction of graphs.
"""

import getopt 

from . import graph
from .core import reaxsample, reaxmap
from .stats import dump 
from .util import parse 
from .util import cmdhandle
from .util import iohandle


def display_species_info(mapgraph, species):
    
    print('species %s:'%species)
    print('Stability:\t%f\nAve Conc:\t%f\nTotal flux:\t%f'%(
            mapgraph.graph.node[species]['stability'],
            mapgraph.graph.node[species]['c'],
            mapgraph.graph.node[species]['totalflow']
            ))

    if species not in mapgraph.flow_graph.node:
        print('Species %s is unselected.' % species)
        return

    print('Element selected: %s' % mapgraph.element)
    print('Source:%f' % mapgraph.flow_graph.node[species]['inflow'])
    if mapgraph.graph.node[species]['c0'] != 0.0:
        print('%.2f%%\t%.3e\toriginal' % (
            mapgraph.flow_graph.node[species]['c0']/mapgraph.flow_graph.node[species]['inflow']*100,
            mapgraph.flow_graph.node[species]['c0']
        ))
    for src in sorted(
        [e[0] for e in mapgraph.flow_graph.edges() if e[1] == species],
        key=lambda x:mapgraph.flow_graph[x][species]['incontrib'],
        reverse=True
        ):
        print('%.2f%%\t%.3e\t%d reactions\t%s' % (
            mapgraph.flow_graph[src][species]['incontrib']*100,
            mapgraph.flow_graph[src][species]['flow'],
            len(mapgraph.reaction_graph[src][species]),
            src
            ))
    print('Destination:%f\t%.2f%%' % (
        mapgraph.flow_graph.node[species]['outflow'],
        mapgraph.flow_graph.node[species]['outflow']/mapgraph.flow_graph.node[species]['inflow']*100
        ))
    for dst in sorted(mapgraph.flow_graph[species], key=lambda x:mapgraph.flow_graph[species][x]['outcontrib'], reverse=True):
        print('%.2f%%\t%.3e\t%d reactions\t%s' % (
            mapgraph.flow_graph[species][dst]['outcontrib']*100,
            mapgraph.flow_graph[species][dst]['flow'],
            len(mapgraph.reaction_graph[species][dst]),
            dst
            ))
    print('Remains:%f\t%.2f%%'%(
        (mapgraph.flow_graph.node[species]['inflow'] - mapgraph.flow_graph.node[species]['outflow']),
        (mapgraph.flow_graph.node[species]['inflow'] - mapgraph.flow_graph.node[species]['outflow'])/mapgraph.flow_graph.node[species]['inflow']*100
        ))


def display_reactions(mapgraph, src, dst):
    """ return reactions and ratio between src and dst.
    """
    try:
        print('Total direct flux from %s to %s:%e' % ( src, dst, mapgraph.flow_graph[src][dst]['flow']))
    except KeyError:
        print('no reaction from %s to %s' % (src, dst))
        return
    try:
        print('ratio\t\tflux\trate\tname')
        for attr in sorted(mapgraph.reaction_graph[src][dst].values(), key=lambda x:abs(x['contrib']), reverse=True):
            print('% .2f%%\t\t% .3e\t% .3e\t%s'%(
                    attr['contrib']*100.0,
                    attr['flux'],
                    attr['r'],
                    attr['reaction']
                    ))
    except KeyError:
        print('None')


def _main(argv):
    rmap = None
    output = None
    interactive = False 

    absolute = False 
    element = 'mass'
    reverse = False 
    slice_ = slice(None)

    help_str = """
Usage: mapcut show [OPTION] ... [FILE] (,FILE2)

OPTION:
--abs: If set, dump absolute value rather than relative value;
-e [element]: Select element to calculate path flow. Default is `mass`.
-i: Set to enter interact mode;
-m [reaxmap]: Filename of reaxmap. If set, only use reactions and species described in the map;
-s [slice]: Slice string, such as '1:2'. Slice sample or rawdata;
-o [output]: Output filename. Default is stdout;
--rev: Treat reversible reaction as two seperated reactions;
-h, --help: Display this help.

FILE, FILE2:
If only one filename is given, treat it as a Reaction Sample File. If two filenames are given, 
treat them as Reaction Data Dump Files."""

    ia_help_str = """
dump [filename]: Dump json to file;
element [element]: Rebuild flow graph based on element;
group [element] (group_id): Assign a group to species with specific element;
info [species_name]: Get species information;
reaction [species1] [species2]: Get reaction reaction information between two species;
refresh: Rebuild flow graph;
select [species1,species2,...] (group_id): Assign a group to species;
ungroup: Remove all groups.
    """

    opts, args = getopt.getopt(argv[1:], 'e:him:s:o:', ['help', 'rev', 'abs'])

    for opt, arg in opts:
        if opt == '-e':
            element = arg 
        elif opt == '-i':
            interactive = True 
        elif opt == '-m':
            rmap = reaxmap.read_file(arg)
        elif opt == '-s':
            slice_ = parse.parse_slice(arg)
        elif opt == '-o':
            output = arg
        elif opt == '-h' or opt == '--help':
            print(help_str)
            return 1
        elif opt == '--rev':
            reverse = True 
        elif opt == '--abs':
            absolute = True 

    assert args, 'Error: No data is given.'

    if len(args) == 1:
        file_dataobj = reaxsample.read_file(args[0], verbose=True)[slice_]
    elif len(args) == 2:
        file_dataobj = dump.read_file(args[0], args[1], verbose=True)[slice_]

    mapgraph = graph.MapGraph()
    mapgraph.load(file_dataobj, rmap)
    mapgraph.build_flow_graph(element=element, count_reverse=reverse)

    if not interactive:
        mapgraph.to_json(output, absolute=absolute)
        return 0
    else:
        cmdhandle.input_init(None, [
            'd', 'dump', 'e', 'element', 'exit', 'g', 'group', 'h', 'help', 'i', 'info',
            'q', 'quit', 'refresh', 'r', 'reaction', 's', 'select', 'ungroup'], 
            'mapshow> ')

    while True:
        cmd, fargs = cmdhandle.read_command_from_input(' ')
        if cmd in ['q', 'exit', 'quit']:
            break

        try:
            if cmd in ['d', 'dump']:
                mapgraph.to_json(iohandle.get_list(fargs, 0, output))
            elif cmd in ['e', 'element']:
                mapgraph.build_flow_graph(fargs[0])
            elif cmd in ('g', 'group'):
                mapgraph.group_element(fargs[0], iohandle.get_list_ex(fargs, 1, 1, int))
            elif cmd in ['i', 'info']:
                display_species_info(mapgraph, fargs[0])
            elif cmd in ['h', 'help']:
                print(ia_help_str)
            elif cmd in ['r', 'reaction']:
                display_reactions(mapgraph, fargs[0], fargs[1])
            elif cmd == 'refresh':
                mapgraph.refresh()
            elif cmd in ['s', 'select']:
                mapgraph.group_species(fargs[0].split(','), iohandle.get_list_ex(fargs, 1, 1, int))
            elif cmd == 'ungroup':
                mapgraph.ungroup()
            else:
                print('Command %s does not exist.' % cmd)
        except Exception as e:
            print(e)


if __name__ == '__main__':
    main(sys.argv[1:])
