
import numpy as np
import networkx as nx
import json
import re
import sys
import getopt
import itertools

from .core.reaxsample import ReaxSample
from .stats.dump import ReaxRawData
from .core.reaxmap import ReaxMap
from .util.smiles import get_element_dict
from .util.reaction import rev_reaxstr, split_reaxstr, canon_reaxstr
from .util.cmdhandle import read_command_from_input


class MapGraph(object):
    """ Visualizer of `ReaxSample`/`ReaxRawData`.

    Example:

    ```
    > import mapcut
    > reaxmap = mapcut.ReaxMap(['A', 'B', 'C'])
    > reaxmap.add_reaction('A+B=C')
    > g = mapcut.MapGraph()
    > g.load(reaxmap)
    > g.build_flow_graph()
    > g.to_json('reaction.json')
    ```
    """

    def __init__(self):
        self.graph = nx.MultiDiGraph() # complete graph, including forward and backward
        self.flow_graph = nx.DiGraph() # species graph
        self.c_init = None  # initial rate (as reference)
        self.c_ave = None   # average rate (as reference)
        self.r_sum = None   # sum of reaction rate

    def load(self, data_obj, rmap=None):
        """ Load data from object.

        Args:
            data_obj: Can be `ReaxSample` or `ReaxRawData` instance.
            rmap: ReaxMap instance. If not None, use species and reactions listed above.
        """

        species = rmap.CheckSpecies() if rmap else None 
        reactions = rmap.CheckReactions() if rmap else None

        if isinstance(data_obj, ReaxSample):
            self._load_sample(data_obj.net_rate_sample(), species, reactions)
        elif isinstance(data_obj, ReaxRawData):
            self._load_rawdata(data_obj, species, reactions)
        else:
            raise NotImplemented

    def _load_rawdata(self, rawdata, species=None, reactions=None):
        self.c_init = dict(zip(rawdata.species(), rawdata.c_dump.iloc[0].as_matrix()))
        self.c_ave = dict(zip(rawdata.species(), rawdata.c_dump[1:].mean().as_matrix()))
        self.r_sum = dict(zip(rawdata.reactions(), rawdata.r_dump[1:].sum().as_matrix() * 
            (rawdata.r_dump.index[1] - rawdata.r_dump.index[0])))
        self.include_reverse = rawdata.include_reverse
        self._build_graph(species=species, reactions=reactions)

    def _load_sample(self, rsample, species=None, reactions=None, reverse=False):
        
        self.c_init = rsample.init
        for s in rsample.species():
            if not s in self.c_init:
                self.c_init[s] = 0.0

        self.c_ave = dict(zip(rsample.species(), rsample.c.mean().as_matrix()))
        self.r_sum = dict(zip(rsample.reactions(), rsample.r.sum().as_matrix() * 
            (rsample.r.index[1] - rsample.r.index[0])))
        self.include_reverse = rsample.include_reverse
        self._build_graph(species=species, reactions=reactions)        

    def _build_graph(self, species=None, reactions=None):

        if species is None:
            species = list(self.c_ave.keys())
        if reactions is None:
            reactions = list(self.r_sum.keys())

        self.reactions = reactions

        # add nodes
        for s in species:
            self.graph.add_node(s, **{
                'c':self.c_ave[s],          # average concentration
                'c0':self.c_init[s],        # init concentration
                'in':self.c_init[s],        # inflow
                'out':0.0,                  # outflow
                'elements':get_element_dict(s, True),
                'group':1                   # group id, default 1
                })
        
        # set raw attr
        for rxn in reactions:

            if self.r_sum[rxn] >= 0.0:
                actual_name = rxn
                actual_rate = self.r_sum[rxn]
            else: # an actual-reverse reaction
                actual_name = rev_reaxstr(rxn)
                actual_rate = -self.r_sum[rxn]

            actual_reactants, actual_products = split_reaxstr(actual_name)
            for s in actual_reactants:
                self.graph.node[s]['out'] += actual_rate
            for s in actual_products:
                self.graph.node[s]['in'] += actual_rate

        # stability
        for name, node_attr in self.graph.node.items():
            node_attr['remain'] = node_attr['in'] - node_attr['out']                        # how many remains in the end
            node_attr['increase'] = node_attr['in'] - node_attr['out'] - node_attr['c0']    # net increase
            node_attr['totalflow'] = node_attr['in'] + node_attr['out']                     # in & out
            if node_attr['c'] == 0:
                node_attr['stability'] = 0.0
            elif node_attr['in'] + node_attr['out'] == 0.0:
                node_attr['stability'] = 10.0   # avoid Infinity
            else:
                node_attr['stability'] = node_attr['c']/(node_attr['in'] + node_attr['out'])

    def build_flow_graph(self, element, count_reverse=False):
        """ Generating graph representing the flow of elements.

        Args:
            element: The name of element. If 'mass', use mole mass as measurement;
            count_reverse: Treat reversible reaction as two seperated reactions.
        """
        self.element = element
        self.flow_graph = nx.DiGraph() # clear
        self.reaction_graph = nx.MultiDiGraph()

        for s, attr in self.graph.node.items():
            if attr['group'] == 0:
                continue 

            weight = float(attr['elements'].get(element, 0.0))

            self.flow_graph.add_node(s,
                name=s,
                group=attr['group'],
                weight=weight,
                c0=attr['c0'] * weight,
                inflow=attr['c0'] * weight,
                outflow=0.0,
                stability=attr['stability']
            )
            self.reaction_graph.add_node(s)

        count_reverse = count_reverse and self.include_reverse
        reactions_canon = set([canon_reaxstr(r) for r in self.reactions]) if not count_reverse else self.reactions

        for rxn in reactions_canon:
            
            if count_reverse or not self.include_reverse:
                rate = self.r_sum[rxn]  # seperate reverse reactions
            else:
                rate = self.r_sum[rxn] - self.r_sum[rev_reaxstr(rxn)]
    
            if rate == 0.0:
                continue 

            actual_rate = abs(rate)
            actual_name = rxn if rate > 0.0 else rev_reaxstr(rxn)

            actual_reactants, actual_products = split_reaxstr(actual_name)

            # remove repetition (hope everything happened only once)
            for s in set(actual_reactants).intersection(actual_products):
                actual_reactants.remove(s)
                actual_products.remove(s)

            sum_weight = sum((self.flow_graph.node[s]['weight'] for s in actual_reactants))
        
            for u, v in itertools.product(actual_reactants, actual_products):
                flux = actual_rate * self.flow_graph.node[u]['weight'] * self.flow_graph.node[v]['weight'] / sum_weight
                if flux == 0:
                    continue 
                self.reaction_graph.add_edge(u, v, reaction=actual_name, flux=flux, r=actual_rate)
                self.reaction_graph.add_edge(v, u, reaction=rev_reaxstr(actual_name), flux=-flux, r=-actual_rate)
            
        # summary
        for src, dst_dict in self.reaction_graph.adjacency():
            if self.graph.node[src]['group'] == 0:
                continue

            for dst, key_dict in dst_dict.items():
                if self.graph.node[dst]['group'] == 0:
                    continue

                flow = sum([attr['flux'] for attr in key_dict.values()])
                if flow >= 0.0:
                    self.flow_graph.add_edge(src, dst, flow=flow)
                    self.flow_graph.node[src]['outflow'] += flow 
                    self.flow_graph.node[dst]['inflow'] += flow
               
                # contribution of reaction to a flow
                for attr in key_dict.values():
                    attr['contrib'] = attr['flux']/flow

        for n in self.flow_graph.node.values():
            n['totalflow'] = n['inflow'] + n['outflow']

        # flow contribution
        for src, dst in self.flow_graph.edges():
            self.flow_graph[src][dst]['incontrib'] = self.flow_graph[src][dst]['flow']/self.flow_graph.node[dst]['inflow']
            self.flow_graph[src][dst]['outcontrib'] = self.flow_graph[src][dst]['flow']/self.flow_graph.node[src]['inflow']


    def to_json(self, output=None, absolute=False):
        """ Write graph as json format.

        Args:
            output: filename, default is `stdout`;
            absolute: set `True` to dump absolute value;
        """

        # normalization maximum flow
                
        max_flow = max([
            self.flow_graph[src][dst]['flow'] for src, dst in self.flow_graph.edges()
            ]) if not absolute else 1.0 

        for src, dst in self.flow_graph.edges():
            self.flow_graph[src][dst]['weight'] = self.flow_graph[src][dst]['flow']/max_flow        

        graph_to_draw = nx.DiGraph()
        i = 0
        for node in self.flow_graph.node:
            self.flow_graph.node[node]['idx'] = i
            graph_to_draw.add_node(i, **self.flow_graph.node[node])
            i += 1

        for src, dst in self.flow_graph.edges():
            graph_to_draw.add_edge(self.flow_graph.node[src]['idx'], self.flow_graph.node[dst]['idx'], **self.flow_graph[src][dst])

        f = open(output, 'w') if output is not None else sys.stdout 
        json.dump(nx.readwrite.json_graph.node_link_data(graph_to_draw), f, indent=2)
        if f is not sys.stdout:
            f.close()

    def group_element(self, element, group_idx=1):
        """ Assign group index to species containing element specified.

        Args:
            element: `str` representing element.
        """
        self.select_species([s for s in self.graph.node if element in s], group_idx)

    def group_species(self, selections, group_idx=1):
        """ Assign group index to species specified. Species not in selection will be ungrouped.

        Args:
            selections: list of `str` containing species to be selected.
            group_idx: target group index.
        """
        for species in self.graph.node:
            if species in selections:
                self.graph.node[species]['group'] = group_idx
            elif self.graph.node[species]['group'] == group_idx:
                self.graph.node[species]['group'] = 0

    def ungroup(self):
        """ Ungroup all species.
        """
        for attr in self.graph.node.values():
            attr['group'] = 1

    def refresh(self):
        """ Re-calculate the flow data between species.
        """
        self._build_flow_graph()


def rsample2json(rsample, rmap=None, output=None, absolute=False, element='mass', **kwargs):
    ''' Simple wrap for dumping json for ReaxSample.

    Args:
        rsample: `ReaxSample` instance;
        rmap: `ReaxMap` instance. If is not `None`, only use species and reactions appeared in reaxmap;
        output: filename, use `stdout` if is `None`;
        absolute: Use absolute value for edge weight instead of relative value;
        element: target element or `mass`.

    Kwargs:
        count_reverse: `bool`, if `True`, treating reversed reaction as two reactions.
    '''
    mapgraph = MapGraph()
    mapgraph.load(rsample, rmap)
    mapgraph.build_flow_graph(element, kwargs.get('count_reverse', False))
    mapgraph.to_json(output, absolute)
