
from .util.parse import parse_opt

from .core import reaxmap, reaxsample
from .stats import dump
from .reduce import reduce_map
from .autoreduce import auto_reduce
from .series import Series
from .util.iohandle import check_opt


def main(argv):
    
    args, kwargs = parse_opt(argv[1:], {'a':'args', 's':'sample', 'e':'method', 't':'time', 'k':'keep', 'o':'output', 'h':'help'}, multiple=True)
    check_opt(args, kwargs, opt_required=['method'], help_document="""
Usage: mapcut reduce [OPTION] ... [FILE] ...

OPTION:
-a, --args: Arguments that passed to reduction method.
-s, --sample: Reaxsample file. Only one sample is allowed.
-k, --keep: Species to keep. Used in DRG-like methods.
-o, --output: Filename of reduced reaxmap output.
-e, --method: Method used;
-t, --time: Interval of each reaxmap that holds within, if there are multiple maps.
--auto: Automatic reduction.
--output_sample: Filename to write reaxsample that returned in automatic reduction.
-h, --help: Display this help.

Additional options will be passed to reduction methods as keyword arguments.

FILE:
Reaction kinetic data files.""")

    rmaps = [reaxmap.read_file(arg) for arg in args]
    rsample = reaxsample.read_file(kwargs.pop('sample')) if 'sample' in kwargs else None

    method = kwargs.pop('method')
    reduce_args = kwargs.get('args', list())
    if not isinstance(reduce_args, list):
        reduce_args = [reduce_args]

    if len(rmaps) == 1: # single map
        
        rmap = rmaps[0]
        rmap.set_priority(kwargs['keep'])

        if not 'auto' in kwargs:
            assert len(args) > 0, 'No enough arguments'
            result = reduce_map(rmap, rsample, method, *reduce_args, **kwargs)[0]

        else:
            rawdata_ref = dump.read_file(*kwargs.pop('rawdata'))
            result, result_sample = auto_reduce(rmap, rsample, method, 'all', None, rawdata_ref=rawdata_ref, **kwargs)[:2]

        result.write_file(kwargs.get('output', None))


    else:

        series = Series(rmaps, kwargs['time'])
        series.set_priority(kwargs['keep'])

        if not 'auto' in kwargs:
            assert len(args) > 0, 'No enough arguments'
            result = series.reduce_all(rsample, method, *reduce_args, **kwargs)

        else:
            rawdata_ref = dump.read_file(*kwargs.pop('rawdata'))
            result, maps, result_sample = auto_reduce(series, rsample, method, 'all', None, rawdata_ref=rawdata_ref, mode='series', **kwargs)[0:3]
            
        for r, name in zip(result.toreaxmaplist(), args):
            r.write_file(name.rsplit('.', 1)[0] + kwargs.get('output', '-cut.rkd'))
