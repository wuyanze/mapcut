import unittest
import numpy as np
import numpy.testing as nt

import sys
sys.path.append('../..')

from mapcut.series import Series
from mapcut.core.reaxmap import ReaxMap

class SeriesTest(unittest.TestCase):

    def test_construction(self):

        rmap1 = ReaxMap(['b', 'a'])
        rmap1.add_reaction('a=b', (0.2, 0.1))

        rmap2 = ReaxMap(['c'])
        rmap2.add_reaction(['b=a', 'b=c'], [(0.3, 0.4), (0.5, 0.6)])

        series = Series([rmap1, rmap2], [10, 10])

        self.assertListEqual(['a', 'b', 'c'], series.rmaps[0].CheckSpecies())
        self.assertListEqual(['a', 'b', 'c'], series.rmaps[1].CheckSpecies())
        self.assertListEqual(['a=b', 'b=c'], series.rmaps[0].CheckReactions())
        self.assertListEqual(['a=b', 'b=c'], series.rmaps[1].CheckReactions())
        nt.assert_array_equal([0.2, 0.0], series.rmaps[0].toarray()[2])
        nt.assert_array_equal([0.1, 0.0], series.rmaps[0].toarray()[3])
        nt.assert_array_equal([0.4, 0.5], series.rmaps[1].toarray()[2])
        nt.assert_array_equal([0.3, 0.6], series.rmaps[1].toarray()[3])


    def test_run(self):

        rmap1 = ReaxMap(['a', 'b'])
        rmap1.add_reaction('a=b', (1.0, 0.0))
        rmap2 = ReaxMap(['a', 'b'])
        rmap2.add_reaction('a=b', (2.0, 0.0))

        series = Series([rmap1, rmap2], [1.0, 1.0])
        runner = series.run_all(0.01, {'a':1.0}, None, None, 1)
        nt.assert_array_almost_equal(np.array([0.04987, 0.9502]), runner.results[-1][0], decimal=3)


if __name__ == '__main__':
    unittest.main()