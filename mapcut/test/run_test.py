# test running modules

import unittest
import numpy as np
import numpy.testing as nt

import sys
sys.path.append('../..')

from mapcut.maprun import MapRunner, _run_system
from mapcut.core import ReaxMap
from mapcut.core.reaxmap import Priority

class RunTest(unittest.TestCase):

    def test_corerun(self):
        mat_r = np.array([[1, 1, 0]])
        mat_p = np.array([[0, 0, 1]])
        kp = np.array([0.1])
        km = np.array([0.0])
        c0 = np.array([1.0, 1.0, 0])
        
        c, r = _run_system(mat_r, mat_p, kp, km, c0, 10.0, 1.0, net=True)
        self.assertEqual(len(c), len(r))
        self.assertEqual(11, len(c))

        nt.assert_array_almost_equal(c[0], c0)
        nt.assert_array_almost_equal(c[-1], np.array([0.5, 0.5, 0.5]), decimal=4)

    def test_begin(self):
        runner = MapRunner()
        runner.reaxmap = ReaxMap(species=['H2', 'O2', 'H2O'])
        runner.set_c0({'H2':2.0, 'O2':1.0})
        nt.assert_array_equal(runner.c_init, np.array([2.0, 1.0, 0.0]))
        
    def test_run(self):
        runner = MapRunner()
        runner.c_init = np.array([2.0, 1.0, 0.0])
        runner.run_args = (np.array([[2, 1, 0]]), np.array([[0, 0, 2]]), np.array([1.0]), np.array([0.1]))
        runner.dump_file = None
        runner.run(10, 1.0)
        self.assertEqual(len(runner.results), 11)
        
    def test_dump(self):
        runner = MapRunner()
        runner.load(self.setup_reaxmap())

        runner.set_dump('tmp', 2, species=['H2O', 'H2'])
        nt.assert_array_equal([True, False, True], runner.dump_mask)
        self.assertEqual(runner.dump_args, (2, 1e-10))

        runner.results = [
            (np.array([1.0, 2.0, 3.0]), None),
            (np.array([1.0, 2.0, 3.0]), None),
            (np.array([1.0, 2.0, 3.0]), None)
            ]
        runner.timestep = 1.0
        runner.dump(2)
        runner.dump_file.close()

    def test_loadmap(self):
        runner = MapRunner()
        runner.load(self.setup_reaxmap())

        mat_r, mat_p, kp, km = runner.run_args
        self.assertEqual(mat_r.shape[0], 1)
        self.assertEqual(mat_p.shape[0], 1)
        self.assertEqual(len(kp), 1)
        nt.assert_array_equal(mat_r, [np.array([2, 1, 0])])
        nt.assert_array_equal(mat_p, [np.array([0, 0, 2])])
       
        self.assertEqual(kp[0], 1.0)
        self.assertEqual(km[0], 0.1)

    def test_restart(self):
        runner = MapRunner()
        runner.reaxmap = ReaxMap(species=['a', 'b', 'c'])
        runner.results = [((1, 2, 3), None)]
        runner.timestep = 0.1
        runner.last_dump_idx = 10
        runner.write_restart('tmp')
        with open('tmp', 'r') as f:
            lines = [line.strip('\n').split('=') for line in f.readlines()]
            self.assertEqual(['t', 'a', 'b', 'c'], [l[0] for l in lines])
            nt.assert_array_equal([1, 1, 2, 3], [float(l[1]) for l in lines])
        
        runner.read_restart('tmp')
        nt.assert_array_equal([1,2,3], runner.c_init)

    def setup_reaxmap(self):
        reaxmap = ReaxMap(species=['H2', 'O2', 'N2', 'H2O'])
        reaxmap.set_priority('N2', Priority.IGNORED)
        reaxmap.set_priority('H2', Priority.CRITICAL)

        reaxmap.add_reaction('H2+H2+O2=H2O+H2O', k=(1.0, 0.1))
        reaxmap.add_reaction('H2=O2', k=(2.0, 0.2))

        reaxmap.reaction_priority[1] = Priority.IGNORED

        return reaxmap


if __name__ == '__main__':
    unittest.main()

