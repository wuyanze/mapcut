import sys
sys.path.append('../..')

from mapcut.reduce import share, drg, rop, pymars

import unittest
import numpy as np
import numpy.testing as nt


class Test_coretest(unittest.TestCase):

    def test_reduce_map2graph(self):
        adjReactants = np.array(
            [[1, 2], [0, 1], [0, 2]])
        adjProducts = np.array(
            [np.array([2, 2]), np.array([2]), np.array([1])])
        weight = np.array([0.1, 0.2, 0.3])
        result = np.array(
            [[0, 0.3, 0.2], [0, 0, 0.4], [0, 0.3, 0.2]]
            )
        graph_abs = share.Map2Graph(adjReactants, adjProducts, weight, 3, 'abs').toarray()
        nt.assert_array_equal(result + result.transpose(), graph_abs)
        
        graph_net = share.Map2Graph(adjReactants, adjProducts, weight, 3, 'net').toarray()
        nt.assert_array_equal(result - result.transpose(), graph_net)

        result_cos = np.array(
            [[-0.5,-0.5,-0.5], [0.1,0,0], [-0.1,0,0]]
            )
        graph_cos_net = share.Map2Graph(adjReactants, adjProducts, weight, 3, 'cos', 'net').toarray()
        nt.assert_array_almost_equal(result_cos, graph_cos_net)

        result_cos_abs = np.array(
            [[0.5,0.5,0.5], [0.5,0.6,0.6], [0.5,0.6,0.6]]
            )
        graph_cos_abs = share.Map2Graph(adjReactants, adjProducts, weight, 3, 'cos', 'abs').toarray()
        nt.assert_array_almost_equal(result_cos_abs, graph_cos_abs)


    def test_reduce_drgscan(self):
        graph = np.array(
            [[0, 1.0, 0, 0], [0.4, 0, 0.4, 0.2], [0, 1.0, 0, 0], [0, 1.0, 0, 0]]
            )
        ipt_s = np.array([8, 4, 4, 4])
        ipt_r = np.array([4, 4, 4])
        list_r = np.array(
            [np.array([0]), np.array([1]), np.array([1])]
            )
        list_p = np.array(
            [np.array([1]), np.array([2]), np.array([3])]
            )
        new_ipt_s, new_ipt_r = drg.DRGScan(graph, 0.3, (ipt_s, ipt_r, list_r, list_p))[0:2]
        nt.assert_array_equal(np.array([8, 4, 4, 0]), new_ipt_s)
        nt.assert_array_equal(np.array([4, 4, 0]), new_ipt_r)

    def test_reduce_drgsan2(self):
        graph = np.array(
            [[1, 0, 0, 0], [0, 1, 1, 0], [0, 1, 1, 0], [0, 0, 0, 1]]
            )
        ipt_s = np.array([8, 8, 4, 4])
        ipt_r = np.array([])
        list_r = np.array([])
        new_ipt_s = drg.DRGScan(graph, 0, (ipt_s, ipt_r, list_r, list_r))[0]
        nt.assert_equal(np.array([8, 8, 4, 0]), new_ipt_s)

    def test_reduce_drgep(self):
        list_r = np.array(
            [np.array([0]), np.array([0]), np.array([1])])
        list_p = np.array(
            [np.array([1]), np.array([2]), np.array([2])])
        rate = np.array([0.5, 1, 1])
        mat_graph = drg.GetDRGEP(list_r, list_p, 4, rate)
        nt.assert_array_almost_equal(mat_graph, np.array(
            [[1,1/3,2/3,0.01], [0.5,1,1,0.01], [0.5,0.5,1,0.01], [0.01,0.01,0.01,1]]), 2)

    def test_reduce_restore(self):
        list_r = np.array([[0], [0], [0]])
        list_p = np.array([[1], [2], [4]])
        ipt_s = np.array([4, 4, 0, 8, 4])
        ipt_r = np.array([0, 0, 0])
        remain_r = [0, 1]
        new_ipt_s, new_ipt_r = share.Restore_reaction(remain_r, ipt_s, ipt_r, list_r, list_p)[0:2]
        nt.assert_array_equal(new_ipt_r, np.array([4, 4, 0]))
        nt.assert_array_equal(new_ipt_s, np.array([4, 4, 4, 8, 0]))

    def test_reduce_rop(self):
        list_r = np.array([[0],[0]])
        list_p = np.array([[1],[2]])
        rate = np.array([0.1,-0.3])
        rop_, trop = rop.GetNormROP(list_r, list_p, 3, rate)
        nt.assert_array_almost_equal(trop, np.array([[0.25,0.75],[1, 0],[0, 1]]))
        nt.assert_array_almost_equal(rop_, np.array([[-1, 1],[1, 0], [0, -1]]))
        new_ipt_s = rop.cspi_cut((np.array([4,4,4]), np.array([4,4]), list_r, list_p), [None, rate], 0.5, True)[0]

    def test_reduce_pymars_drg(self):
        list_r = np.array(
            [np.array([0]), np.array([0]), np.array([1])])
        list_p = np.array(
            [np.array([1]), np.array([2]), np.array([2])])
        rate = np.array([0.5, 1, 1])
        ipt_s = np.array([8, 4, 4])        
        ipt_r = np.array([4, 4, 4])

        pymars.drg_cut((ipt_s, ipt_r, list_r, list_p), (None, rate), 0.1)

    def test_reduce_pymars_get_rate_data(self):
        adjReactants = np.array(
            [[1, 2], [0, 1], [0, 2]])
        adjProducts = np.array(
            [np.array([2, 2]), np.array([2]), np.array([1])])
        weight = np.array([-10, 20, 30])

        edge_data = pymars.wrappers.get_rate_data_drg(([None]*3, None, adjReactants, adjProducts), (None, weight))[0][0]

        self.assertDictEqual(edge_data[1], {'0':50, '1':60, '2':60})
        self.assertDictEqual(edge_data[2], {'0_1':50, '0_2':50, '1_2':60, '1_0':50, '2_0':50, '2_1':60})

        edge_data2 = pymars.wrappers.get_rate_data_drgep(([None]*3, None, adjReactants, adjProducts), (None, weight))[0][0]

        self.assertDictEqual(edge_data2[1], {'0':50, '1':40, '2':40})
        self.assertDictEqual(edge_data2[2], {'0_1':50, '0_2':50, '1_2':20, '1_0':10, '2_0':10, '2_1':20})


if __name__ == '__main__':
    unittest.main()
