import unittest
import sys
import numpy as np 

sys.path.append('../..')

from mapcut import autoreduce

class AutoreduceTest(unittest.TestCase):

    def test_next_iter(self):

        # all left

        x = [0.0, 1.0]
        y = [-1.0, -0.5]

        np.testing.assert_allclose((2.0, 1.0), autoreduce.next_iter(x, y, 0.0, 3.0, 'linear_double'))

        # all left (boundary)

        np.testing.assert_allclose((0.9, 0.0), autoreduce.next_iter(x, y, 0.0, 0.9, 'linear_double'))

        # left & right

        y = [-1.0, 1.0]
        np.testing.assert_allclose((0.5, 0.5), autoreduce.next_iter(x, y, 0.0, 1.0, 'linear_double'))

        # multiple left & right (good case)

        x = [0.0, 1.0, 2.0, 3.0]
        y = [-0.5, -0.2, 0.2, 0.5]
        np.testing.assert_allclose((1.5, 0.5), autoreduce.next_iter(x, y, 0.0, 3.0, 'linear_double'))

        # multiple left & right (select left)

        x = [0.0, 1.0, 2.0, 3.0]
        y = [-2.0, -1.5, -0.5, 1.0]
        np.testing.assert_allclose((2.5, 0.5), autoreduce.next_iter(x, y, 0.0, 3.0, 'linear_double'))

        # ill data
        x = [0.0, 1.0, 2.0, 3.0]
        y = [-2.0, -0.5, -1.0, 1.0]
        np.testing.assert_allclose((2.5, 0.5), autoreduce.next_iter(x, y, 0.0, 3.0, 'linear_double'))
        

if __name__ == '__main__':
    unittest.main()
