import sys
import unittest 
import numpy as np 
import pandas as pd 

sys.path.append('../..')

from mapcut.stats import ratelaw 

# Note: This test case will only test validity of functions,
# but parameters need to be fine tuning

class RateTest(unittest.TestCase):
    
    def test_axfunction(self):
        a = np.array([1,2,3])
        b = np.array([1,1,1])

        np.testing.assert_array_equal([1,3,4,1], ratelaw._add_np_array(
            a, b, 1
        ))

        a = np.array([1,2,3,4,5,6]) 
        np.testing.assert_array_equal([1,3,5], ratelaw._resample(a, 2))

        a = np.array([1,1,2,3,3,3,4])
        b = np.array([1,2,3,3,3,3,5])
        np.testing.assert_array_equal([1,1,2,3,4], ratelaw._remove_redundant_data(
            a, b
        )[0])

        a = np.array([1,2,3])
        b = np.array([4,5,6])

        self.assertEqual(True, ratelaw._is_proportional(a, b, 0.01, 0, 1))

    def test_rateconstant(self):
        
        x = np.array([1,2,3])
        y = np.array([4,5,6])
        

        np.testing.assert_allclose([2.0, 0.2], list(ratelaw._rate_constant(y, x, 'div')), atol=1e-6)
        np.testing.assert_allclose([1.0, 0.0], list(ratelaw._rate_constant(y, x, 'fit')), atol=1e-6)
        np.testing.assert_allclose([16/7, 9/35], list(ratelaw._rate_constant(y, x, 'fit0')), atol=1e-6)

        rd = ratelaw.ReaxRawData(
            c_dump=pd.DataFrame([1, 2, 3], index=[1,2,3], columns=['a']),
            r_dump=pd.DataFrame(np.ones((3,2)), index=[1,2,3], columns=['a+a=a', 'a=a+a']),
            include_reverse=True)

        rmap = ratelaw.rawdata2reaxmap(rd)
        self.assertEqual(rmap.CheckSpecies(), ['a'])
        self.assertEqual(rmap.CheckReactions(), ['a=a+a'])

        np.testing.assert_allclose([0.5, 3/14], list(rmap.reactions[0].k), atol=1e-6)


    def test_reaxstat_load(self):
        rd = ratelaw.ReaxRawData(
            c_dump=pd.DataFrame([1, 2, 3], index=[0, 0.1,0.2], columns=['a']),
            r_dump=pd.DataFrame(np.ones((3,2)), index=[0, 0.1, 0.2], columns=['a+a=a', 'a=a+a']),
            include_reverse=True)

        rstat = ratelaw.ReaxStat()
        rstat.load(rd)
        rstat.load(rd, add='para')
        rstat.submit()

        self.assertEqual(0.1, rstat.timestep)
        self.assertSetEqual({'a=a+a','a+a=a'}, set(rstat.reactions.keys()))
        np.testing.assert_array_equal(
            [[2,2,2], [2,4,6]],
            rstat.reactions['a=a+a']
        )
        np.testing.assert_array_equal(
            [[2,2,2], [2,8,18]],
            rstat.reactions['a+a=a']
        )

    def test_reaxstat_build_reaxmap(self):
        rstat = ratelaw.ReaxStat()
        rmap = rstat._build_reaxmap({
            'a+a=a':{'k':1.0, 'select':False},
            'a=a+a':{'k':1.0, 'select':True}
        })
        self.assertEqual(['a'], rmap.CheckSpecies())
        self.assertEqual(['a=a+a'], rmap.CheckReactions())
        self.assertEqual((1.0, 0), rmap.reactions[0].k)

if __name__ == '__main__':
    unittest.main()
