import sys
sys.path.append('../..')

from mapcut.util import blockfile, cmdhandle, errhandle, errors, iohandle, numeric, reaction, unit, parse

import unittest

class UtilTest(unittest.TestCase):


    def test_blockfile_readblockfile(self):
        with open('tmp', 'w') as f:
            f.write('A\na\tb #123\nc d\nEND\nB\nEND\nC\ne\tf\nEND\n')
        block = blockfile.ReadBlockFile('tmp', ['A','B','C', 'END'])
        self.assertEqual(len(block), 3)
        self.assertEqual(block['A'], [('a', ['b']), ('c d', [])])
        self.assertEqual(block['B'], [])
        self.assertEqual(block['C'], [('e', ['f'])])
        print('ReadblockFile passed')

    def test_blockfile_writeblockfile(self):
        block = {'A':[('a', ['b']), ('c d', [])], 'B':[], 'C':[('e', ['f'])]}
        blockfile.WriteBlockFile('tmp', block)
        with open('tmp', 'r') as f:
            self.assertEqual(f.read(128), 'A\na\tb\nc d\nEND\nB\nEND\nC\ne\tf\nEND\n')
        print('WriteBlockFile passed')

    def test_cmdhandle_handlecommandline(self):
        cmd, args = cmdhandle.handle_command_line('a b  c # def')
        self.assertEqual(cmd, 'a')
        self.assertEqual(args, ['b', 'c'])

        cmd, args = cmdhandle.handle_command_line('a ')
        self.assertEqual(cmd, 'a')
        self.assertEqual(args, [])

        
    def test_cmdhandle_readcommandfromline(self):
        with open('tmp', 'w') as f:
            f.write('a b c \n')
            f.write('d\n')
            f.write(' # 123\n')
            f.write('\n')
            f.write('e f')

        func_list = cmdhandle.read_command_from_file('tmp')
        self.assertEqual(len(func_list), 3)
        self.assertEqual(func_list[0], ('a', ['b', 'c']))
        self.assertEqual(func_list[1], ('d', []))
        self.assertEqual(func_list[2], ('e', ['f']))

    def test_iohandle_dictarray(self):
        a = {1:2, 3:6, 5:4}
        self.assertListEqual(iohandle.dict2array(a), [1,5,3])

    def test_numeric(self):
        import numpy as np 
        a = np.array([[1,2,3],[4,5,6]])

        np.testing.assert_array_equal(np.array([[0, 0, 0], [4, 5, 6]]), 
            numeric.MatFilter(a, 4))
        np.testing.assert_array_equal(np.array([[0, 2, 3], [4, 5, 6]]),
            numeric.MatRowFilter(a, 0.2)
        )

        adj_list =[np.array([]), np.array([1,2]), np.array([1, 1])]
        np.testing.assert_array_equal(np.array(([0, 0, 0], [0, 1, 1], [0, 2, 0])),
            numeric.List2Mat(adj_list).toarray()
        )

    def test_parse(self):
        
        self.assertEqual(parse.parse_num('123'), 123)
        self.assertEqual(parse.parse_num('abc'), 'abc')
        with self.assertRaises(ValueError):
            parse.parse_num('abc', True)

        self.assertEqual(parse.parse_slice('1'), slice(1, 2))
        self.assertEqual(parse.parse_slice('1:2'), slice(1, 2))
        self.assertEqual(parse.parse_slice('1:2:3'), slice(1,2,3))

        self.assertEqual(parse.parse_token('1', flag='basic'), 1)
        self.assertEqual(parse.parse_token('1e3', flag='basic'), 1e3)
        self.assertEqual(parse.parse_token('None', flag='basic'), None)
        self.assertEqual(parse.parse_token('true', flag='basic'), True)
        self.assertEqual(parse.parse_token('1,2,3', flag='smart'), [1,2,3])

        self.assertDictEqual(parse.parse_dict(['1=b', 'a=4']), {'1':'b', 'a':4})

    def test_reaction(self):
        
        reaction_str = 'b+a=d+c'
        self.assertEqual((['b', 'a'], ['d', 'c']), reaction.split_reaxstr(reaction_str))
        self.assertEqual('a+b=c+d', reaction.canon_reaxstr(reaction_str))
        self.assertEqual('d+c=b+a', reaction.rev_reaxstr(reaction_str))

        r = reaction.Reaction(reaction_str, [1.0, 2.0])
        self.assertEqual(reaction_str, str(r))
        r.canonicalize()
        self.assertEqual('a+b=c+d', str(r))


if __name__ == '__main__':
    unittest.main()
