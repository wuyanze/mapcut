import sys
sys.path.append('../..')

from mapcut.core import reaxmap, reaxsample

import unittest
import numpy as np 
import pandas as pd 

class CoreTest(unittest.TestCase):

    def test_reaxmap_construct(self):

        species = ['a', 'b', 'c']
        reactions = ['a+b=c', 'c=b+b']
        ks = [(1.0, 2.0), (2.0, 1.0)]

        rmap = reaxmap.ReaxMap(species=species)

        self.assertSetEqual(set(species), set(rmap.species.keys()))
        for r, k in zip(reactions, ks):
            rmap.add_reaction(r, k)

        self.assertListEqual(['a+b=c','b+b=c'], rmap.prior_reactions())
        self.assertTupleEqual((1.0, 2.0), rmap.reactions[1].k)
        
        self.assertListEqual([reaxmap.Priority.NORMAL]*3,
            [rmap.species_priority[i] for i in rmap.species])

        self.assertListEqual([reaxmap.Priority.NORMAL, reaxmap.Priority.NORMAL],
            rmap.reaction_priority
        )

        rmap.set_priority('b', reaxmap.Priority.IGNORED)
        rmap.set_priority('c', reaxmap.Priority.CRITICAL)
        self.assertListEqual(['a', 'c'], rmap.prior_species())
        self.assertListEqual(['c'], rmap.prior_species(reaxmap.Priority.CRITICAL))

        rmap2 = rmap.skeleton()
        self.assertListEqual(['a', 'c'], list(rmap2.species))

    def test_reaxmap_io(self):

        species = ['a', 'b', 'c']
        reactions = ['a+b=c', 'c=b+b']
        ks = [(1.0, 2.0), (2.0, 1.0)]
        rmap = reaxmap.ReaxMap(species=species)
        rmap.add_reaction(reactions, ks)

        rmap.write_file('tmp')
        with open('tmp', 'r') as f:
            s = f.read(256)
            self.assertEqual('SPECIES\na\nb\nc\nEND\nREACTIONS\na+b=c\t1\t2\nb+b=c\t1\t2\nEND\n', s)
        
        rmap2 = reaxmap.read_file('tmp')
        self.assertEqual(rmap2.species, rmap.species)
        self.assertEqual(rmap2.reactions, rmap.reactions)

    def test_reaxmap_encoding(self):
        species = ['a', 'b', 'c']
        reactions = ['a+b=c', 'c=b+b']
        ks = [(1.0, 2.0), (2.0, 1.0)]
        rmap = reaxmap.ReaxMap(species=species)
        rmap.add_reaction(reactions, ks)

        rlist, plist = rmap.stoi_mat()
        self.assertEqual(2, len(rlist))
        self.assertEqual(2, len(plist))
        np.testing.assert_array_equal(np.array([np.array([0, 1]), np.array([1, 1])]), rlist)
        np.testing.assert_array_equal(np.array([np.array([2]), np.array([2])]), plist)

        sipt, ript = rmap.EncodeNumArray()[0:2]

        np.testing.assert_array_equal([4,4,4], sipt)
        np.testing.assert_array_equal([4,4], ript)

        rmap.DecodeNumArray((np.array([0,4,4]), np.array([0,4])))
        self.assertListEqual([0,4,4], [rmap.species_priority[s] for s in rmap.species])
        self.assertListEqual([0,4], rmap.reaction_priority)
        self.assertListEqual(['b', 'c'], rmap.CheckSpecies())
        self.assertListEqual(['b+b=c'], rmap.CheckReactions())

    def test_reaxsample_construct(self):
        c = pd.DataFrame([[2,1,0],[4,3,0]], index=[100, 200], columns=['b','a','c'])
        r = pd.DataFrame([[5,6],[7,8]], index=[100, 200], columns=['a+b=c','c=b+b'])        
        init = {'a':9, 'b':10}

        rsample = reaxsample.ReaxSample(c=c, r=r, init=init)

        rsample.sort_names()

        self.assertListEqual(['a','b','c'], rsample.species())
        
        np.testing.assert_array_equal([100,200], rsample.t())
        self.assertEqual(2, len(rsample))

        c_, r_ = rsample.at(0)
        np.testing.assert_array_equal([1,2,0], c_)
        np.testing.assert_array_equal([5,6], r_)

        c_, r_ = rsample.at(0, c_slice=['a'], r_slice=['a+b=c'])
        np.testing.assert_array_equal([1], c_)
        np.testing.assert_array_equal([5], r_)

        np.testing.assert_array_equal([1,3], rsample.concentration('a'))

        rsample2 = rsample[0:1]
        self.assertEqual(1, len(rsample2))
        np.testing.assert_array_equal([100], rsample2.t())

    def test_reaxsample_canon(self):
        c = pd.DataFrame([[2,1,0],[4,3,0]], index=[100, 200], columns=['b','a', 'c'])
        r = pd.DataFrame([[5,6],[7,8]], index=[100, 200], columns=['a+b=c','c=b+a'])        
        init = {'a':9, 'b':10}
        rsample = reaxsample.ReaxSample(c=c, r=r, init=init)

        rsample.canon_reaction_names()

        self.assertListEqual(['a+b=c', 'c=a+b'], rsample.reactions())
        np.testing.assert_array_equal([6, 8], rsample.rate('c=a+b'))
        np.testing.assert_array_equal([-1, -1], rsample.rate('a+b=c', count_reverse=True))

        netsample = rsample.net_rate_sample()
        self.assertListEqual(['a+b=c'], netsample.reactions())
        np.testing.assert_array_equal([-1, -1], netsample.rate('a+b=c'))

    def test_reaxsample_io(self):
        c = pd.DataFrame([[2,1,0],[4,3,0]], index=[100, 200], columns=['b','a', 'c'])
        r = pd.DataFrame([[5,6],[7,8]], index=[100, 200], columns=['a+b=c','c=b+b'])
        init = {'a':9, 'b':10}

        rsample = reaxsample.ReaxSample(c=c, r=r, init=init);
        rsample.write_file('tmp')

        with open('tmp', 'r') as f:
            s = f.read(256)
            self.assertEqual('SPECIES\nt,b,a,c\n100,2,1,0\n200,4,3,0\nEND\nREACTIONS\nt,a+b=c,c=b+b\n100,5,6\n200,7,8\nEND\nINIT\na,9.0\nb,10.0\nEND\n',
                s
            )
        
        rsample2 = reaxsample.read_file('tmp')
        np.testing.assert_array_equal(c, rsample2.c.as_matrix())
        np.testing.assert_array_equal([[5,-6],[7,-8]], rsample2.r.as_matrix())
        self.assertDictEqual(init, rsample2.init)

    def test_reaxmap_sort(self):
        species = ['c', 'b', 'a']
        reactions = ['c=b+b', 'a+b=c']
        rmap = reaxmap.ReaxMap(species=species)
        rmap.add_reaction(reactions, None)
        rmap.set_priority('c', reaxmap.Priority.CRITICAL)
        rmap.reaction_priority[0] = reaxmap.Priority.CRITICAL
        
        rmap.sort()

        self.assertListEqual(['a', 'b', 'c'], rmap.CheckSpecies())
        self.assertListEqual(['a+b=c', 'b+b=c'], rmap.CheckReactions())
        
        ipts, iptr = rmap.EncodeNumArray()[:2]

        self.assertListEqual([4, 4, 8], ipts.tolist())
        self.assertListEqual([4, 8], iptr.tolist())


if __name__ == '__main__':
    unittest.main()
