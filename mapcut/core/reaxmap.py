
import sys
import re
import numpy as np
from collections import OrderedDict, Iterable

from ..util import errors
from ..util import errhandle
from ..util import unit as _unit
from ..util import blockfile
from ..util.reaction import Reaction, species_set, canon_reaxstr
from ..util.iohandle import array2dict


class Priority:

    IGNORED = 0
    NORMAL = 4
    CRITICAL = 8


class ReaxMap(object):
    """ A container that stores the kinetic information (species, reactions, rate constants)
        for a reaction system.

    Example:
    ```
    > from mapcut import ReaxMap
    > r = ReaxMap(['a', 'b', 'c'])
    > r.add_reaction(['a+2b=c', 'a=b'])
    > r
    (reaxmap with 3 species and 2 reactions)
    > r.prior_species()
    ['a', 'b', 'c']
    > r.prior_reactions()
    [2b+a=c, a=b]
    > r.index_reaction('c=2b+a')
    2b+a=c
    ```
    """

    def __init__(self, species=None, **kwargs):
        """ 
        Args:
            species: list-like object, containing name of species;
        """
        if species:
            self.species = OrderedDict.fromkeys(species)
            self.species_priority = OrderedDict([(s, Priority.NORMAL) for s in self.species])
            assert len(self.species) == len(species), 'Species repeat'
        else:    
            self.species = OrderedDict()       # dict of dicts
            self.species_priority = OrderedDict()

        self.reactions = []     # list of reactions
        self.reaction_priority = []


    def __repr__(self):
        return '(reaxmap with %d species and %d reactions)' % (len(self.CheckSpecies('all')), len(self.CheckReactions()))

    def _validity_check(self):
        # check validity
        assert len(self.CheckReactions()) == len(set(self.CheckReactions())), 'Reactions repeat'
        assert set(self.species).issuperset(set(species_set(self.CheckReactions()))), 'Reaction has unknown species'

    def add_species(self, name, priority=Priority.NORMAL):
        """ Add one species.

        Args:
            name: Name of species;
            priority: Integer represents the importance.
        """
        self.species[name] = None
        self.species_priority[name] = priority

    def add_reaction(self, name, k=None, **kwargs):
        """ Add one new reaction. The newly added reaction will be automatically canonialized.

        Args:
            k: Rate constant, `None` or tuple of `float` (forward k, backward k).
        """
        if isinstance(name, str):
            self.reactions.append(Reaction(name, k, **kwargs))
            self.reactions[-1].canonicalize()
            self.reaction_priority.append(Priority.NORMAL)
        else:
            if not k:
                for n in name:
                    self.add_reaction(n)
            else:
                for n, k_ in zip(name, k):
                    self.add_reaction(n, k_)

    def CheckReactions(self, reactionList=None):
        """ Check if a list of reactions is valid. If `reactionList` is `None`, same as `prior_reactions()`.
        """
        if reactionList is None:
            return [str(r) for r in self.prior_reactions()]
        else:
            return [str(r) for r in reactionList if r in self.prior_reactions()]

    def CheckSpecies(self, option='all', speciesList=None):
        """ Check a list of string is valid species names, or return species with certain priority.

        Args:
            option: Can take the following values:
                'all', return all species with priority >= NORMAL
                'critical', return all species with priority >= CRITICAL
                'regex', only return species that match with certain regular expression
                'species', return species in `speciesList` (including ignored species)
        """
        if option == 'all':
            return [s for s in self.prior_species()]

        elif option == 'critical':
            return [s for s in self.prior_species(Priority.CRITICAL)]

        elif option == 'regex':
            return [s for s in self.species if re.search(speciesList[0], s)]

        elif option == 'species':
            for s in speciesList:
                errhandle.assert_in(s, self.species)
                if self.species_priority[s] == Priority.IGNORED:
                    print('Warning: species %s is ignored' % s, file=sys.stderr)
            return speciesList

        else:
            raise errors.InvalidArgument(option)

    def DecodeNumArray(self, numMapArray, kcArray=None):
        """ Decode numerical tuples generated by `EncodeNumArray()`.

        Args:
            numArrayTuple: full length numarray generated by `EncodeNumArray()`;
            kcArray: If the reactions are changed, reload reactants, products and kcarray.
        """

        if kcArray:
            raise NotImplementedError()

        for s, i in zip(self.species_priority, numMapArray[0]):
            self.species_priority[s] = i

        self.reaction_priority = numMapArray[1].tolist()

    def EncodeNumArray(self):
        """ Encoding a reaxmap into tuple of four elements: 
            
            array of species priorities,
            array of reaction priorities,
            adjacent matrix of reactants, generated by `stoi_mat()`,
            adjacent matrix of products, generated by `stoi_mat()`.

        Note:
            Ignored species is still included in reactant/product adjacent matrices.
        """
        reactant_list, product_list = self.stoi_mat(full=True)
        return np.array(list(
            self.species_priority.values())), np.array(
            self.reaction_priority), reactant_list, product_list

    def index_reaction(self, reaction_name):
        """ Index a reaction by name.

        Args:
            reaction_name: `str`, name of reaction, like ``A+B=C``.

        Returns:
            `reaction` object.
        """
        name_1, is_reverse_1 = canon_reaxstr(reaction_name, True)
        for reaction in self.reactions:
            if reaction == name_1:
                return reaction
        raise KeyError(reaction_name)

    def prior_species(self, priority=Priority.NORMAL):
        """ Return species and attributes which has higher priority than `priority`.
        """
        return [s for s in self.species if self.species_priority[s] >= priority]

    def prior_reactions(self, priority=Priority.NORMAL):
        """ Return list of reaction object which has higher priority than `priority`.
        """
        return [self.reactions[i] for i in range(len(self.reaction_priority)) 
                    if self.reaction_priority[i] >= priority]

    def set_priority(self, species, priority=Priority.CRITICAL):
        """ Set priority of certain species.

        Args:
            species: `str` or list of `str`, name of species proceeded;
            priority: `int`, priority required, by default is ``CRITICAL``.
        """
        if isinstance(species, str):
            self.species_priority[species] = priority
        else:
            for s in species:
                self.species_priority[s] = priority

    def skeleton(self, trim_unused_reactions=False):
        """ Return a `ReaxMap` containing important species and reactions.

        Args:
            trim_unused_reactions: If set, remove reactions with rate constant = 0, and corresponding species.
        """

        rmap = ReaxMap()
        if not trim_unused_reactions:
            for name, priority in self.species_priority.items():
                if priority >= Priority.NORMAL:
                    rmap.add_species(name, priority)

            for i in range(len(self.reactions)):
                if self.reaction_priority[i] >= Priority.NORMAL:
                    rmap.reactions.append(self.reactions[i])
                    rmap.reaction_priority.append(self.reaction_priority[i])

        else:
            for i in range(len(self.reactions)):
                if self.reaction_priority[i] >= Priority.NORMAL and (self.reactions[i].k[0] != 0.0 or self.reactions[i].k[1] != 0.0):
                    rmap.reactions.append(self.reactions[i])
                    rmap.reaction_priority.append(self.reaction_priority[i])

            for name in species_set(rmap.CheckReactions()):
                if self.species_priority[name] >= Priority.NORMAL:
                    rmap.add_species(name, self.species_priority[name])

        return rmap

    def sort(self):
        """ Sort the species and reactions by alphabetical order.
        """

        self.species = OrderedDict.fromkeys(sorted(self.species.keys()))
        self.species_priority = OrderedDict(zip(self.species.keys(), [self.species_priority[s] for s in self.species]))
        reaction_package = list(zip(self.reactions, self.reaction_priority))
        reaction_package.sort(key=lambda x:str(x[0]))
        self.reactions, self.reaction_priority = tuple(zip(*reaction_package))
        self.reactions = list(self.reactions)
        self.reaction_priority = list(self.reaction_priority)

    def stoi_mat(self, full=False):
        """ Return adjacent matrix of stoichiometric coefficients in reactant side/product side, 
        with reaction index as row index and species index as column index.

        Args:
            full: If set to `True`, return all reactions, including ignored reactions.

        Example:
        ```
        > rmap = ReaxMap(['a', 'b', 'c'])
        > rmap.add_reaction('a+2b=c')
        > rmap.stoi_mat()
        (array([[1,2,0]]), array([0,0,1]))
        ```        
        """

        reactant_list = []
        product_list = []
        
        species_id = array2dict(self.species.keys())

        for reaction in self.prior_reactions() if not full else self.reactions:
            reactant_list.append(np.array([species_id[s] for s in reaction.reactants]))
            product_list.append(np.array([species_id[s] for s in reaction.products]))

        return np.array(reactant_list), np.array(product_list)

    def toarray(self, kinetics=True):
        """ Encode the `ReaxMap` into arrays. Ignored species will not be included.

        Returns:
            reactant_list: Adjacent matrix of reactants, generated by `stoi_mat()`;
            product_list: Adjacent matrix of products, generated by `stoi_mat()`;
            kp, kn: Array of forward and backward rate constants. Only returned when `kinetics` is set to `True`.
        """
        reactant_list, product_list = self.stoi_mat()
        
        if not kinetics:
            return reactant_list, product_list
        else:
            prior_reaction = self.prior_reactions()
            kp = np.array([reaction.k[0] for reaction in prior_reaction])
            kn = np.array([reaction.k[1] for reaction in prior_reaction])
            return reactant_list, product_list, kp, kn

    def write_file(self, filename, unit=None, formatter='%.8g', verbose=True):
        """ Write reaction kinetic data file.

        Args:
            filename: Name of the destination file, or `None` to output to `stdout`.
            unit: Unit format. Can take the following value:
                'atomic': A/ps/1
                'si': m/s/mol
                'cgs': cm/s/mol
                All numbers in `ReaxMap` are regarded to have 'atomic' unit, so reading
                an RKD file with different unit would cause incorrect results. 
            formatter: Format of float numbers.
            verbose: Print things while output.
        """

        blocks = OrderedDict(SPECIES=sorted(self.prior_species()))
                  
        if unit == 'atomic' or not unit:
            blocks['REACTIONS'] = [
                (str(r), [formatter%r.k[0], formatter%r.k[1]]) for r in self.prior_reactions()
                ]
        elif unit == 'si':
            def k_2_si(k, len_r):
                return k / (_unit.c_2_si ** (len_r-1) * _unit.time_2_si)

            blocks['REACTIONS'] = [
                (str(r), (
                    formatter% k_2_si(r.k[0], len(r.reactants)), 
                    formatter% k_2_si(r.k[1], len(r.products)))) for r in self.prior_reactions()
                ]

        elif unit == 'cgs':
            def k_2_cgs(k, len_r):
                return k / (_unit.c_2_cgs ** (len_r-1) * _unit.time_2_cgs)

            blocks['REACTIONS'] = [
                (str(r), (
                    formatter% k_2_cgs(r.k[0], len(r.reactants)),
                    formatter% k_2_cgs(r.k[1], len(r.products)))) for r in self.prior_reactions()
            ]

        else:
            raise errors.InvalidArgument(unit, 'unit', 'write_file')

        blocks['REACTIONS'].sort(key=lambda x:x[0])

        blockfile.WriteBlockFile(filename, blocks)
        if verbose:
            print('Reaxmap(%d species and %d reactions) written to %s' % (
                len(self.prior_species()),
                len(self.prior_reactions()),
                filename if filename is not None else 'stdout'), file=sys.stderr)


def read_file(filename, verbose=True):
    """ Read a reaction kinetic data (RKD) file.

    Args:
        filename: Name of RKD file;
        verbose: Print information to `sys.stderr` while reading.

    Returns:
        reaxmap: `ReaxMap` instance.
    """

    blocks = blockfile.ReadBlockFile(filename, ['SPECIES', 'REACTIONS', 'ELEMENTS', 'END'])

    rmap = ReaxMap(species=[name for name, arg in blocks['SPECIES']])
    for reaction, k in blocks['REACTIONS']:
        rmap.add_reaction(reaction, (float(k[0]), float(k[1])))

    rmap._validity_check()

    if verbose:
        print(rmap, file=sys.stderr)
    return rmap
