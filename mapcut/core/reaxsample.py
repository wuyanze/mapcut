""" Data handling module.
"""

import sys 
import numpy as np
from collections import OrderedDict

import pandas

from . import reaxmap
from ..util import errhandle
from ..util import blockfile
from ..util import reaction 
from ..util.reaction import canon_reaxstr


class ReaxSample:
    """ Stores segment averaged data of species concentration and reaction rate.
    `ReaxSample` stores two tables, concentration table and rate table, which records
    concentration and rate data at different time points.

    Example:

    ```
    > import pandas as pd
    > from mapcut import ReaxSample
    > conc = pd.DataFrame({'a':[10.0,5.0], 'b':[8.0,3.0], 'c':[1.0,6.0]}, index=[10.0, 20.0])
    > rate = pd.DataFrame({'a+b=c':[3.0, 2.0]}, index=[10.0, 20.0])
    > sample
    (2 samples with 3 species and 1 reactions)
    > sample.t()
    array([ 10.,  20.])
    > sample['a']
    array([ 10.,   5.])
    > sample['a+b=c']
    array([ 3.,  2.])
    > subsample = sample[0:1]
    > subsample
    (1 samples with 3 species and 1 reactions)
    > subsample.t()
    array([ 10.])
    ```
    """
    
    def __init__(self, c=None, r=None, init=None, include_reverse=True):
        """ 
        Args:
            c: `pandas.DataFrame`, with sample time points as indices and species names as column names;
            r: `pandas.DataFrame`, with sample time points as indices and reaction names as column names;
            init: `dict`, initial concentration;
            include_reverse: Include reverse reaction.
        """
        self.r = r
        self.c = c
        self.init = init
        self.include_reverse = include_reverse 
        if self.r is not None and self.c is not None:
            self._validity_check()

    def __len__(self):
        return len(self.c)

    def __repr__(self):
        return '(%d samples with %d species and %d reactions)' % (
            len(self.r), len(self.c.columns), len(self.r.columns))

    def __getitem__(self, indexer):
        if isinstance(indexer, slice):
            return self.subsample(indexer)
        elif isinstance(indexer, str):
            if reaction.EQU_SYMBOL in indexer:
                return self.index_reaction(indexer)
            else:
                return self.concentration(indexer)
        else:
            return ValueError(indexer)

    def _validity_check(self):
        # check validity
        assert len(self.r) == len(self.c), 'Sample length not match'
        assert (self.r.index == self.c.index).all(), 'Time stamp not match'
        assert len(self.species()) == len(set(self.species())), 'Species repeat'
        assert len(self.reactions()) == len(set(self.reactions())), 'Reactions repeat'
        assert set(self.species()).issuperset(reaction.species_set(self.reactions())), 'Reaction has unknown species'

    def at(self, index, c_slice=None, r_slice=None):
        """ Return numerical representation at specific sample point of specific species and reactions.

        Args:
            index: `int`, index of sample list;
            c_slice: `None` (return all species), or list of species names;
            r_slice: `None` (return all reactions), or list of reaction names;

        Returns:
            c_sample: `numpy.array` of all species concentrations;
            r_slice: `numpy.array` of all reaction rates;
        """
        if not c_slice:
            c_sample = self.c.iloc[index].as_matrix()
        else:
            c_sample = self.c.iloc[index][c_slice].as_matrix()
        
        if not r_slice:
            r_sample = self.r.iloc[index].as_matrix()
        else:
            r_sample = self.r.iloc[index][r_slice].as_matrix()
        
        return c_sample, r_sample

    def canon_reaction_names(self):
        """ Canonicalize all reaction names.
        """
        # determine if there is reverse
        new_name_and_is_reverse = [canon_reaxstr(r, True) for r in self.r.columns]
        new_names = [n[0] for n in new_name_and_is_reverse]

        self.include_reverse = (len(set(new_names)) != len(new_names))

        if self.include_reverse:  # if include reverse, just change name partially
            new_columns = [reaction.canon_reaxstr_without_reverse(r) for r in self.r.columns]
        
        else:                   # otherwise change the symbol of rate data
            new_columns = new_names 
            for i, is_reverse in enumerate([n[1] for n in new_name_and_is_reverse]):
                if is_reverse:
                    self.r.iloc[:, i] *= -1

        self.r.columns = new_columns

    def compare_with(self, source):
       """ Compare concentration with source list.
        Args:
            source: `ReaxSample` object

        Returns: `dict` with entrys below:
            species_sub: Species appear in source but not here;
            reactions_sub: Reactions appear in source but not here;
            species_add: Species appear here but not in source;
            reactions_add: Reactions appear here but not in source;
            error: dict of concentration deviation (only with species in both samples)
                s: error of species s (dict):
                    max_err: maximum absolute deviation;
                    max_err_re: maximum relative deviation;
                    ave_err: average absolute deviation;
                    ave_err_re: average relative deviation;
                    re_int: integral of relative deviation;
        """

       result = {
            'species_sub': set(source.species()).difference(self.species()),
            'reactions_sub': set(source.reactions()).difference(self.reactions()),
            'species_add': set(self.species()).difference(source.species()),
            'reactions_add': set(self.reactions()).difference(source.reactions())
            }

       species_error = dict()
       for s in source.c:
            if s not in self.c:
                continue
            src_c = source[s]
            dst_c = self[s]
            abs_error = np.abs(dst_c - src_c)
            src_c[src_c == 0] = 1.0
            re_error = np.divide(abs_error, src_c)

            species_error[s] = {
                'max_err':np.max(abs_error),
                'ave_err':np.average(abs_error),
                'max_err_re':np.max(re_error),
                'ave_err_re':np.average(re_error),
                're_int':np.sum(abs_error)/np.sum(src_c)
            }
            
       result['error'] = species_error
       return result

    def concentration(self, species):
        """ Return concentration array of species.

        Args:
            species: `str` or list of `str`.
        """
        return self.c[species].as_matrix()

    def empty(self):
        """ Return if this `ReaxSample` object stores any data.
        """
        return self.c.empty() or self.r.empty()

    def index_reaction(self, reaction_name, count_reverse=False):
        """ Return rate of a specific reaction.
        
        Args:
            reaction_name: `str`, name of reaction, which does not have to be canonicalized.
            count_reverse: Whether treat reversible reactions as two independent reactions, will be
                ignored if reverse reactions are not included.

        Returns:
            rate: `np.array` object.
        """
        if self.include_reverse: # only search same direction
            name_1 = reaction.canon_reaxstr_without_reverse(reaction_name)
            for rxn in self.r.columns:
                if name_1 == reaction.canon_reaxstr_without_reverse(rxn):
                    return self.rate(rxn, count_reverse=count_reverse)

        else: # search forward and back
            name_1, is_reverse_1 = reaction.canon_reaxstr(reaction_name, True)
            for rxn in self.r.columns:
                name_2, is_reverse_2 = reaction.canon_reaxstr(rxn, True)
                if name_1 == name_2:
                    return self.rate(rxn, count_reverse=count_reverse) * (-1 if is_reverse_1^is_reverse_2 else 1)
        raise IndexError(reaction_name)

    def net_rate_sample(self):
        """ Returns a duplicate reference of `ReaxSample` object, in which the forward
        and backward reaction are treated as one reaction with net reaction rate.

        Returns:
            `ReaxSample` object.

        Note:
            This object will not call `copy()`, which means the new `ReaxSample` object will
            still share same concentration data.
        """
        if not self.include_reverse:
            return self 
        else:
            new_r_dict = OrderedDict()
            for rxn in self.r.columns:
                name_1, reverse_1 = reaction.canon_reaxstr(rxn, True)
                if name_1 in new_r_dict:
                    new_r_dict[name_1] += self.r[rxn] * (-1 if reverse_1 else 1)
                else:
                    new_r_dict[name_1] = self.r[rxn] * (-1 if reverse_1 else 1)
            new_sample = ReaxSample(
                c=self.c, 
                r=pandas.DataFrame(new_r_dict, index=self.r.index),
                init=self.init,
                include_reverse=False)
            return new_sample

    def rate(self, reaction_name, count_reverse=False):
        """ Return net rate of specific reactions.

        Args:
            reaction_name: `str` of list of `str`, canonicalized reaction string.
            count_reverse: Whether treat reversible reactions as two independent reactions, will
                be ignored if reverse reactions are not included.

        Returns:
            rate: `np.array`, net rate of specific reactions.
        """
        if count_reverse and self.include_reverse:
            return self.r[reaction_name].as_matrix() - self.r[reaction.rev_reaxstr(reaction_name)].as_matrix()
        else:
            return self.r[reaction_name].as_matrix()

    def reactions(self):
        """ Return list of reactions.
        """
        return self.r.columns.tolist()

    def sort_names(self):
        """ Sort species and reaction names by alphabetical order.
        """
        self.c = self.c.reindex(columns=sorted(self.c.columns))
        self.r = self.r.reindex(columns=sorted(self.r.columns))

    def subsample(self, slice_):
        """ Return a time slice of this `ReaxSample` instance.

        Args:
            slice_: `slice` object

        Note:
            This object will not call `copy()`, which means the new `ReaxSample` object will
            still share same concentration data.
        """
        if (slice_.start == 0 or not slice_.start) and (slice_.stop == len(self) or not slice_.stop):
            return self
        else:
            if slice_.start is None or slice_.start == 0:
                new_init = self.init
            else:
                new_init = dict(zip(self.species(), self.c.iloc[slice_.start - 1].as_matrix()))
            return ReaxSample(self.c.iloc[slice_], self.r.iloc[slice_], init=new_init)

    def species(self):
        """ Return list of species
        """
        return self.c.columns.tolist()

    def t(self):
        """ Return array of time points.
        """
        return self.c.index.values

    def write_file(self, filename, verbose=True):
        """ Write the `ReaxSample` object to file.

        Args:
            filename: `str`, name of file;
            verbose: If set, print things while writing.
        """

        blocks = OrderedDict()
        block_c = [('t', self.species())]
        for i in range(len(self.c)):
            block_c.append((self.c.index[i], self.c.iloc[i].tolist()))
        block_r = [('t', self.reactions())]
        for i in range(len(self.r)):
            block_r.append((self.r.index[i], self.r.iloc[i].tolist()))
        
        blocks['SPECIES'] = block_c
        blocks['REACTIONS'] = block_r
        blocks['INIT'] = [(name, [float(c)]) for name, c in self.init.items()]
        blockfile.WriteBlockFile(filename, blocks, ',')
        if verbose:
            print('Reaxsample written to', filename, file=sys.stderr)
    

def read_file(filename, verbose=True, norm_reaction=True, sep=','):
    """ Read reaxsample from file stored.

    Args:
        filename: Name of input.
        verbose: `bool`, display information or not.
        norm_reaction: Canonicalize reaction names; Otherwise, read reaction names as-is.
        sep: Seperator, default is ','.

    Returns:
        `ReaxSample` instance.
    """

    blocks = blockfile.ReadBlockFile(filename, ['SPECIES', 'REACTIONS', 'INIT', 'END'], split_char=sep)

    sample = ReaxSample()

    sample.c = pandas.DataFrame(
        [list(map(float, row[1])) for row in blocks['SPECIES'][1:]],
        [float(row[0]) for row in blocks['SPECIES'][1:]],
        columns=blocks['SPECIES'][0][1]
        )
    sample.r = pandas.DataFrame(
        [list(map(float, row[1])) for row in blocks['REACTIONS'][1:]],
        [float(row[0]) for row in blocks['REACTIONS'][1:]],
        columns=blocks['REACTIONS'][0][1]
    )
    sample.init = dict([(name, float(c[0])) for name, c in blocks['INIT']])

    # adjust reversed reactions.
    if norm_reaction:
        sample.canon_reaction_names()

    sample._validity_check()

    if verbose:
        print(sample, file=sys.stderr)
    return sample
