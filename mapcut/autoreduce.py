
import copy
import os
import multiprocessing as mp
import warnings
import numpy as np

from .core import reaxmap
from .core import reaxsample
from . import reduce
from . import maprun
from . import series


class ReusedQueue:
    def __init__(self, queue):
        self.queue = queue
        self.has_get = False
        self.result = None

    def get(self):
        if not self.has_get:
            self.result = self.queue.get()
            self.has_get = True

        return self.result
        

def auto_reduce(rmap, rsample, reduce_method, merge_method, sample_method, **kwargs):
    """ Automatic reduce a reaxmap.

    Args:
        rmap: `reaxmap.ReaxMap` instance;
        rsample: `None`/`reaxsample.ReaxSample` instance. If it's `None`, 
            sample is automatically generated from `rawdata_ref` in `kwargs`;
        reduce_method: `str`, method for reduction;
        merge_method: `str`.

    Kwargs:
        accuracy: `float`, accuracy for reduction argument. Default is 0.01;
        comp_method: `str`, sample comparasion method, can be 're_int' (default), 'max_int'.
        cut_args_init: `float`/list of `float`, initial reduction parameter. Default is 0.1.
        fix_method: `str`, argument adjust method, can be 'linear' (default), 'linear_plus', 'linear_double';
        max_cut_arg: `float`, limit of reduction arguments. Default is 0.5;
        max_iter: `int`, max iteration times. Default is 20;
        np: number of processes;
        rawdata_ref: `ReaxRawData` instance for error evaluation;
        rtol: `float`, relative tolerance. Default is 0.1;
        run_interval: running interval, default is 1.0;
        run_time: running time, default is sample size;
        sample_int: `int`/`None`, interval for sampling. If `None`, the interval is used same as `rsample`;
        sample_range: `int`/`None`, range for sampling;
        sample_species: list of `str`, containing species name that used to calculate error;
        sample_weight: list of `int`/`None`, containing weight calculate error.

    Returns:
        mymap: reduced `ReaxMap` instance;
        mysample: `ReaxSample` sampled from kinetic model of reduced map;
        cut_arg: argument used for reduction.
    """
    # read arguments

    def check_list(list_or_object):
        if not isinstance(list_or_object, list):
            return [list_or_object]
        else:
            return list_or_object

    accuracy = kwargs.get('accuracy', 0.01)
    batch_size = int(kwargs.get('batch', 2))
    comp_method = kwargs.get('comp_method', 're_int')
    cut_args = check_list(kwargs.get('cut_args_init', copy.copy([0.1])))
    fix_method = kwargs.get('fix_method', 'linear')
    max_cut_arg = kwargs.get('max_cut_arg', 0.5)
    max_iter = kwargs.get('max_iter', 20)
    mode = kwargs.get('mode', 'single')
    pool_size = kwargs.get('np', os.cpu_count())
    rawdata_ref = kwargs['rawdata_ref']
    rtol = kwargs.get('rtol', 0.1)
    run_interval = kwargs.get('run_interval', 1.0)
    run_time = kwargs.get('run_time', rawdata_ref.t()[-1] + rawdata_ref.t()[1] - rawdata_ref.t()[0])
    sample_int = kwargs.get('sample_int', None)
    sample_range = kwargs.get('sample_range', sample_int)
    sample_species = kwargs.get('sample_species', rmap.CheckSpecies('critical'))
    if not isinstance(sample_species, list):
        sample_species = sample_species.split(',')
    sample_weight = kwargs.get('sample_weight', None)
    verbose = kwargs.get('verbose', True)

    if rsample is None:
        rsample = rawdata_ref.sample(sample_int, sample_range).net_rate_sample()
    else:
        if sample_int is None:
            sample_range = sample_int = int((rsample.t()[1] - rsample.t()[0])/run_interval)

    c_init = rawdata_ref.init_concentration()
    score_args = [sample_species, sample_weight]

    run_results = {}
    cut_results = {0.0: (rmap, None, 0.0)}

    last_scores = [0.0]
    last_cut_args = [0.0]
    batch_cut_args = [cut_args[0]]  # a batch of args used in running
    myaccuracy = accuracy * 2
    iter = 0

    def sample_args():

        pool = mp.Pool(pool_size)
        sample_sessions = []
        print('Args=%r' % batch_cut_args)

        for batch_arg in batch_cut_args:
            if verbose:
                print('Arg=%f' % batch_arg)
                print('Begin cutting session:')
            mymap, map_series = _reduce_object(mode, rmap, rsample, reduce_method, merge_method, *([batch_arg] + cut_args[1:]))
            sample_sessions.append(_sample_object(mode, mymap, sample_int, sample_range, c_init, run_time, run_interval, rawdata_ref, score_args, run_results, pool))
            cut_results[batch_arg] = [mymap, map_series, batch_arg]
            if verbose:
                print('\n')
        
        pool.close()
        pool.join()

        scores = []

        for batch_arg, sample_session in zip(batch_cut_args, sample_sessions):
            mysample, score = sample_session.get()
            scores.append(score)
            if verbose:
                print('Arg=%f, Error=%f' % (batch_arg, scores[-1]))

        if verbose:
            print('\n')
        return scores

    while iter < max_iter:

        iter += 1

        if verbose:
            print('\nIteration = %d\n-----' % iter)

        scores = sample_args()

        last_scores += scores
        last_cut_args += batch_cut_args

        if myaccuracy <= accuracy:
            for arg, score in zip(reversed(batch_cut_args), reversed(scores)):
                if score - rtol < 0:
                    if verbose:
                        print('Difference (%f) < Tolerence (%f). Converged' % (score, rtol))
                        print('Final threshold = %f' % arg)
                    return cut_results[arg]
        
        arg_int, myaccuracy = next_iter(np.array(last_cut_args), np.array(last_scores) - rtol, 0.0, max_cut_arg, fix_method)
        batch_cut_args = interpolate_up(last_cut_args, arg_int, max_cut_arg, batch_size)

    if iter == max_iter:
        warnings.warn('Result not converge at step %d' % max_iter, RuntimeWarning)

    if verbose:
        print('Final threshold is %f' % last_cut_args[-1])

    return cut_results[last_cut_args[-1]]
    

def _reduce_object(mode, rmap_or_series, rsample, reduce_method, merge_method, *args, **kwargs):
    if mode == 'single':
        return reduce.reduce_map(rmap_or_series, rsample, reduce_method, *args, **kwargs)
    elif mode == 'series':
        return rmap_or_series.reduce_all(rsample, reduce_method, *args, **kwargs), None 
    else:
        raise ValueError(mode)


def get_rmap_fingerprint(rmap):

    def get_fingerprint(l):
        s = ''.join(map(str, np.array(l)//2))
        return str(hex(int(s, 5)))

    return get_fingerprint(list(rmap.species_priority.values())) + get_fingerprint(rmap.reaction_priority)


def _sample_object(mode, rmap_or_series, sample_int, sample_range, c_init, run_time, run_interval, rawdata_ref, score_args, run_results, pool):

    if mode == 'single':
        mykey = get_rmap_fingerprint(rmap_or_series)
    elif mode == 'series':
        mykey = ''.join([get_rmap_fingerprint(r) for r in rmap_or_series.rmaps])
    else:
        raise ValueError(mode)

    print('Reaxmap fingerprint:', mykey[:20])

    if mykey in run_results and run_results[mykey]:
        print('[Using exist result]')
        return run_results[mykey]

    else:
        sample_func = _sample_rmap if mode == 'single' else _sample_series
        sample_session = pool.apply_async(sample_func, args=[rmap_or_series, sample_int, sample_range, c_init, run_time, run_interval, rawdata_ref] + score_args)
        run_results[mykey] = ReusedQueue(sample_session)
        return run_results[mykey]


def _sample_rmap(rmap, sample_int, sample_range, c_init, run_time, run_interval, rawdata_ref, *score_args):
    myrunner = maprun.MapRunner()
    myrunner.load(rmap)
    myrunner.set_c0(c_init, strict=False)
    myrunner.run(run_time, run_interval)
    rawdata = myrunner.get_result(trim_last=True)

    return rawdata.sample(sample_int, sample_range), _get_score(rawdata, rawdata_ref, *score_args)


def _sample_series(rseries, sample_int, sample_range, c_init, run_time, run_interval, rawdata_ref, *score_args):
    myrunner = rseries.run_all(run_interval, c_init, None, None)
    rawdata = myrunner.get_result(trim_last=True)
    return rawdata.sample(sample_int, sample_range), _get_score(rawdata, rawdata_ref, *score_args)


def _get_score(rawdata, rawdata_ref, species, weights):
    
    return rawdata.integral_dev(rawdata_ref, species, weights)


def next_iter(x, y, xmin, xmax, method):
    """ Returns next iteration value for equation `y(x) = 0`;

    Args:
        x: x values of previous iterations, follow time order;
        y: y values of previous iterations, follow time order;
        xmin, xmax: range of x;

    Returns:
        x_next: next value of x;
        acc: accuracy;
    """

    def interpolate(x0, y0, x1, y1):
        """ Returns next value of x.
        """
        return (-y0 * x1 + x0 * y1) / (y1 - y0) if y1 != y0 else x0 

    def min_dist(a, v):
        idx = np.searchsorted(a, v)
        if idx == 0:
            return a[0] - v
        elif idx == len(a):
            return v - a[-1]
        else:
            return min(a[idx] - v, v - a[idx-1])

    assert len(x) > 1, 'There must be more than two elements in history'
    assert len(x) == len(y), 'Length of x and y are not equal'

    c = np.array(sorted(zip(x, y), key=lambda f:f[0]))
    x_ = c[:,0]
    y_ = c[:,1]

    # notice: In mapcut, default assumption of y (error) and x (cut arg) is dy/dx > 0
    # so the guess is performed on one side. This may cause infinite loop for ill data. 
    assert y_[0] < 0

    idx1 = np.searchsorted(y_, 0) # first element large than 0
    idx0 = idx1 - 1 # last element less than 0
    ret = None 

    # boundary condition

    if idx1 == len(y): # all elements are less than 0
        if x_[-1] >= xmax: # already larger than xmax
            return xmax, 0.0
        else:
            ret = interpolate(x_[-2], y_[-2], x_[-1], y_[-1])
    elif idx1 == 0: # all elements are larger than 0
        if x_[0] <= xmin:
            return xmin, 0.0
        else:
            ret = max(xmin, interpolate(x_[0], y_[0], x_[1], y_[1]))

    # well-defined data

    else:
        if method == 'linear':
            ret = interpolate(x_[idx0], y_[idx0], x_[idx1], y_[idx1])
        
        # compare three different interpolation methods
        elif method == 'linear_double':
            ret_mid = interpolate(x_[idx0], y_[idx0], x_[idx1], y_[idx1])
            ret_left = x_[idx0] if idx0 == 0 else interpolate(x_[idx0-1], y_[idx0-1], x_[idx0], y_[idx0])
            ret_right = x_[idx1] if idx1 == len(x_)-1 else interpolate(x_[idx1], y_[idx1], x_[idx1+1], y_[idx1+1])

            ret = sorted((
                ret_mid, 
                np.clip(ret_left, x_[idx0], x_[idx1]),
                np.clip(ret_right, x_[idx0], x_[idx1])
                ))[1]

        else:
            raise ValueError(method)

    ret = np.clip(ret, xmin, xmax)
    return ret, min_dist(x_, ret)


def interpolate_up(a, v, vmax, npoints=1):
    """ Returns uniform interpolation between v and the first element in a that
    is larger than v.

    Args:
        a: list-like object;
        v: value;
        vmax: Maximum v, serve as a boundary;
        npoints: number of points returned. If `npoints`=1, return [v].
    """
    
    if v >= vmax:
        return [v]

    a1 = sorted(a) + [vmax] if np.max(a) < vmax else sorted(a)
    v1 = a1[np.searchsorted(a1, v)]

    return [(v1*t + v*(npoints-t))/npoints for t in range(npoints)]
