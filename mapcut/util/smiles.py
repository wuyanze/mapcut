""" A simple script for reading smiles with hydrogen.
"""

elements = ['C', 'H', 'O', 'N', 'S', 'F']
element_mass = {'C':12.01, 'H':1.008, 'O':16.00, 'N':14.01, 'S':32.06, 'F':19.00}

def get_element_dict(name, include_mass=False):

    ret = dict()
    last = None
    for i in name:
        if i in elements:
            if i in ret:
                ret[i] += 1
            else:
                ret[i] = 1
        if i.isdigit():
            if last in ret:
                ret[last] += (int(i) - 1)
        last = i 

    if include_mass:
        ret['mass'] = sum(element_mass[e]*ret[e] for e in ret)

    return ret 


def count_elements(name, element):

    return name.count(element)


def to_formula(name):

    elementdict = get_elements_dict(name)
    return ''.join(('%s%d' % (e, elementdict[e]) for e in elements if e in elementdict))
