""" mapcut error management
    IO Error: raise;
    NotInitialize: Variable has not been initialized; raised by util.errhandle.assert
    NotExist: Key not exist in corresponding data structure; raised by util.errhandle.assert_in
    ValueError: raise;
"""

class MapCutError(Exception):
    def __init__(self, name='', context='', **kwargs):
        self.name = name
        self.context = context


class InvalidArgument(MapCutError):
    """ Invalid argument
    """
    def __init__(self, name = '', context = '', **kwargs):
        return super().__init__(name=name, context=context, **kwargs)


class NotInitialize(MapCutError):
    """ Variable has not been initialized.
    """
    def __init__(self, name='', context='', **kwargs):
        return super().__init__(name=name, context=context, **kwargs)


class NotExist(MapCutError):
    """ Key does not exist in certain data structure.
    """
    def __init__(self, name='', context='', **kwargs):
        return super().__init__(name=name, context=context, **kwargs)
       
