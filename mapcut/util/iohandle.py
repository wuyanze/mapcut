""" Miscalleous functions in list assignment.
"""

def get_list(l, idx, d):
    """ If l[idx] exist, return it, else d;
    """
    try:
        return l[idx]
    except IndexError:
        return d


def get_list_ex(l, idx, d, f):
    """ If l[idx] exist. return f(l[idx]), else d;
    """
    try:
        return f(l[idx])
    except IndexError:
        return d
    

def dict2array(src):
    return sorted(src.keys(), key=lambda x: src[x])


def array2dict(src):
    return dict([(j, i) for i, j in enumerate(src)])


def strip_comment(str, comment_char='#'):
    comment_pos = str.find(comment_char)
    if comment_pos < 0:
        return str.rstrip('\n').strip()
    else:
        return str[:comment_pos].strip()


def check_opt(args:list, kwargs:dict, opt_required, opt_range=None, argc_range=None, help_document=None, help_opt='help'):
    """ Basic check of option input.
    Args:
    ---
    args: list of args;
    kwargs: opt:arg dict;
    opt_required: list of required options;
    opt_range: list of valid options, if `None` any options are valid.
    argc_range: slice of argument range.
    help_opt: Which option is to help.
    """

    if help_opt is not None and help_opt in kwargs:
        print(help_document)
        exit()

    import getopt 

    if argc_range is not None:
        if len(args) >= argc_range.stop:
            raise getopt.GetoptError('Too many arguments')
        elif len(args) < argc_range.start:
            raise getopt.GetoptError('Too few arguments')

    diff_opt = set(opt_required).difference(kwargs)
    if len(diff_opt) > 0:
        raise getopt.GetoptError('Missing option: %s' % diff_opt.pop())

    if opt_range is not None:
        diff_opt = set(kwargs).difference(opt_range)
        if len(diff_opt) > 0:
            raise getopt.GetoptError('Invalid option: %s' % diff_opt.pop())

    
        