"""
Contains functions handling matrix and other math operations.
"""
import numpy as np
import scipy.sparse as sps

def MatFilter(mat, filter, out=None):
    """ Filter value less than a threshold. Mat will not be changed.
    """
    return np.multiply(mat, mat >= filter, out=out)


def MatRowFilter(mat, filter_, division=np.sum, out=None):
    """ For each row, filter row/division(row) >= filter
    Args:
        filter: value to filter;
        division: function to determine divider (can be np.sum or np.max);
    """
    ret = out if out else np.zeros_like(mat)

    for i in range(mat.shape[0]):
        divider = division(mat[i])
        if not divider == 0:
            np.multiply(mat[i], mat[i]/ divider >= filter_, out=ret[i])

    return ret if not out else None 


def List2Mat(adj_list, size=None):
    """ Convert adjacent list to adjacent matrix
    Args:
        adj_list: List of np.array
        size: 
    """
    if not size:
        size = len(adj_list)
    ret = sps.lil_matrix((len(adj_list), size), dtype=adj_list[0].dtype)
    for idx, row in enumerate(adj_list):
        for neighbour in row:
            ret[idx, neighbour] += 1
    return ret
