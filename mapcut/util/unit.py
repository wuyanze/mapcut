import scipy.constants as cons

ATOMIC = 0
SI = 1
CGS = 2

R = cons.R/1000
N_A = cons.N_A
mass_2_si = 0.001
mass_2_real = 1.0
mass_2_cgs = 1.0
length_2_si = 1e-10
length_2_real = 1.0
length_2_cgs = 1e-8
time_2_si = 1e-12
time_2_real = 1000.0
time_2_cgs = 1e-12
volume_2_si = length_2_si**3
volume_2_real = length_2_real**3
volume_2_cgs = length_2_cgs**3
mol_2_si = 1.0/N_A
mol_2_cgs = mol_2_si
c_2_si = mol_2_si/volume_2_si
c_2_real = mol_2_si/volume_2_real
c_2_cgs = mol_2_cgs/volume_2_cgs
energy_2_si = mass_2_si*length_2_si**2/time_2_si**2
energy_2_real = mass_2_real*length_2_real**2/time_2_real**2
