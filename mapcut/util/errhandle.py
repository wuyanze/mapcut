''' Deal with errors. '''

import os.path
import traceback
from . import errors



def check_exist(context, *objs):
    ''' Check if object exist.
    '''
    for obj in objs:
        if not obj:
            raise ValueError('Object missing for %r' % context)


def check_path_exist(path):
    """ Check if path exist, accept list or single str.
    """
    if type(path) is list:
        for p in path:
            if not os.path.exists(p):
                raise IOError(path)
    else:
        if not os.path.exists(path):
            raise IOError(path)


def translate(error, **kwargs):
    """ Translate MapCutError
    """
    str_context = ""
    if error.context:
        str_context = "In %r:\n" % error.context
    
    tb_frame = traceback.extract_tb(error.__traceback__)[-1]
    str_context += "At function %s (%s:%s):\n" % (tb_frame.name, 
        os.path.basename(tb_frame.filename),
        tb_frame.lineno)

    if type(error) == errors.InvalidArgument:
        return "%sInvalid argument: %r" % (str_context, error.name)
    elif type(error) == errors.NotExist:
        return "%s%r does not exist" % (str_context, error.name)
    elif type(error) == errors.NotInitialize:
        return "%s%r has not initialized" % (str_context, error.name)
    else:
        return "%s%s" % (str_context, str(error))


def translate_ex(error, **kwargs):

    tb_frame = traceback.extract_tb(error.__traceback__)[-1]
    str_context = "At function %s (%s:%s):\n" % (tb_frame.name, 
        os.path.basename(tb_frame.filename), 
        tb_frame.lineno)
    for tb_frame in traceback.extract_tb(error.__traceback__)[-2::-1]:
        str_context += '\t\t (%s:%s)\n' % (os.path.basename(tb_frame.filename), tb_frame.lineno)

    if type(error) == ValueError:
        if error.args:
            str_context += "Improper value: %r" % (error.args[0])
    elif type(error) == IOError:
        if error.args:
            str_context += "Cannot open %r" % (error.args[0])
        else:
            str_context += "Cannot open %r" % (error.filename)
    else:
        str_context += 'Error: ' + str(error)

    return str_context

def assert_exist(obj, name='', context=''):
    if not obj:
        raise errors.NotExist(name, context)


def assert_initialized(obj, name='', context=''):
    if not obj:
        raise errors.NotInitialize(name, context)


def assert_in(obj, host, name='', hostname=''):
    if obj not in host:
        raise errors.NotExist(name, hostname)


def mp_error_callback(e):
    raise e 
