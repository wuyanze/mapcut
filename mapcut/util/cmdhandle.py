''' Handle command line input or file input.
'''

import os
import sys 
import shlex
from .iohandle import strip_comment
if os.name != 'nt':
    import readline

HISTORY_NAME = None 
CUSTOM_COMMANDS = []
PROMPT = '> '


def completer(text, state):
    choices = CUSTOM_COMMANDS + os.listdir('.')
    options = [c for c in choices if c.startswith(text)]
    return options[state] if state < len(options) else None 


def input_init(history_name, custom_commands, prompt):
    global HISTORY_NAME, CUSTOM_COMMANDS, PROMPT
    HISTORY_NAME = history_name
    CUSTOM_COMMANDS = custom_commands
    PROMPT = prompt if prompt is not None else PROMPT
    
    if 'readline' in sys.modules:
        if HISTORY_NAME:
            try:
                readline.read_history_file(HISTORY_NAME)
            except IOError:
                pass 
        readline.parse_and_bind('tab: complete')
        readline.set_completer(completer)


def handle_command_line(line, split_char=None, comment_char='#', posix=True):
    """ Split command line.
    If posix is set, then ignore split_char (space) and comment_char ('#') options.
    Returns:
        Command: Command string;
        Args: args string (dos not include argv[0])
    """
    if not posix:
        line = strip_comment(line.strip('\n'), comment_char)
        line_split = line.split(split_char)
    else:
        line_split = shlex.split(line, comments=True)
    if len(line_split) > 0:
        return (line_split[0], line_split[1:])
    else:
        return '', []


def read_command_from_file(filename, split_char=None, comment_char='#'):
    function_list = []

    with open(filename, 'r') as f:
        for line in f.readlines():
            line = strip_comment(line.strip('\n'))
            if not line:
                continue
            function_list.append(handle_command_line(line, split_char, comment_char))
    
    return function_list


def read_command_from_input(split_char=None, comment_char='#'):
    ret = handle_command_line(input(PROMPT), split_char, comment_char)
    if 'readline' in sys.modules and HISTORY_NAME:
        readline.write_history_file(HISTORY_NAME)
    return ret 
