""" Parsing basic types (int, float, bool, list, dict)
"""

def parse_num(string, strict=False):
    """ Convert int/float string. 
    Raise ValueError if 'strict' is set and cannot convert.
    """
    try:
        if '.' in string or 'e' in string:
            return float(string)
        else:
            return int(string)
    except ValueError:
        if strict:
            raise
        else:
            return string


def parse_slice(string):
    """ '1:2:3' ==> slice(1:2:3)
        '1:2' ==> slice(1:2)
        '1' ==> slice(1:2)
    """
    assert string 
    ssplit = string.split(':')

    start = parse_num(ssplit[0], True) if ssplit[0] else None

    if len(ssplit) == 1:
        return slice(start, start+1)

    stop = parse_num(ssplit[1], True) if ssplit[1] else None
    step = parse_num(ssplit[2], True) if len(ssplit) >= 3 else None 

    return slice(start, stop, step)


def parse_token(token, flag='smart', strict=False):
    """ Parse a token using self-defined rules.
    Args:
        token: str. 
        flag: smart, basic, num (must number), str (return directly).
    Parsing rules:
        list: 2,3,4
        keywords: True/true, False/false, None;
        slice: 1:2, :-2.
        integer
        float: If num/num+ is specified, will return float. But num will only return
        float (num+ will return string if cannot parse).
        string.
    """
    if flag == 'str':
        return token

    elif flag == 'num':
        return parse_num(token, strict=True)

    elif flag == 'basic':
        if token in ['True', 'true']:
            return True
        elif token in ['False', 'false']:
            return False 
        elif token in ['None', 'none']:
            return None
        else:
            return parse_num(token, strict=False)

    elif flag == 'smart':
        if '=' in token:
            return parse_dict(token.split(','))
        elif ',' in token:
            return [parse_token(t, flag) for t in token.split(',')]
        elif ':' in token:
            return parse_slice(token)
        else:
            return parse_token(token, 'basic')

    else:
        raise ValueError(flag)


def parse_list(arg_list, flag='basic'):
    """ ['a', 'b'] ==> [a, b]
    """
    return [parse_token(t, flag) for t in arg_list]


def parse_dict(arg_list, flag_val='basic', flag_name='str'):
    ''' ['a=b', 'c=d'] => {a:b, c:d}
    Args:
        arg_list: list of string;
        flag_val: parse flag for values;
        flag_name: parse flag for dict entries;
    '''
    ret = dict()
    for name in arg_list:
        split = name.split('=')
        assert len(split) == 2, 'Invalid dict string: %s' % name

        ret[parse_token(split[0], flag=flag_name)] = parse_token(split[1], flag=flag_val)

    return ret

def parse_args(argv, flag_val='basic', flag_name='str'):
    ''' Parse pythonic function arguments input.
    Args:
        argv: list of string.
        flag_val: parse flag for values;
        flag_name: parse flag for dict entries;
    Returns:
        args: list of arguments;
        kwargs: dict of keyword arguments;
    '''
    kwd_begin = len(argv)
    for i in range(len(argv)):
        if '=' in argv[i]:
            kwd_begin = i
            break
    return (parse_list(argv[:kwd_begin], flag_val), 
            parse_dict(argv[kwd_begin:], flag_val=flag_val, flag_name=flag_name))


def parse_opt(argv, shortopts={}, optrange=None, multiple=False, flag_val='smart'):
    ''' Parse GNU style argument list.
    Args:
        argv: list of string.
        shortopts: dict for short options, {shortopt: keyword}.
        optrange: None/list of string. If list, only opts in this range are allowed.
        multiple: If True, will store arguments of multiple same options into a list.
        flag_val: parse flag for values;
    Returns:
        args: list of arguments;
        kwargs: dict of keyword arguments.
    Example:
        > parse_opt(['-c', '1', 'input', '-name', '--a=2'], shortopts={'c':'cutoff'})
        ['input'], {'cutoff':1, 'name':True, 'a':2}
    '''
    i = 0
    args = []
    kwargs = {}
    opt_count = {}

    def put_opt(opt, arg):
        if opt in opt_count:
            opt_count[opt] += 1
        else:
            opt_count[opt] = 1
        if optrange is not None and opt not in optrange:
            raise ValueError(opt)
        if multiple and opt in kwargs:
            if opt_count[opt] == 2:
                kwargs[opt] = [kwargs[opt]]
            kwargs[opt].append(parse_token(arg))
        else:
            kwargs[opt] = parse_token(arg)

    while i < len(argv):
        if argv[i].startswith('--') or argv[i].startswith('-') and len(argv[i]) > 2:    # long opt
            optarg = argv[i].strip('-')
            if not '=' in optarg: 
                put_opt(optarg, 'True')
            else:
                opt, arg = optarg.split('=', 1)
                put_opt(opt, arg)
            i += 1

        elif argv[i].startswith('-') and len(argv[i]) > 1: # single '-' will be treated as argument
            opt = argv[i].strip('-')
            i += 1
            if i >= len(argv) or argv[i].startswith('-'):
                put_opt(shortopts.get(opt, opt), 'True')
            else:
                put_opt(shortopts.get(opt, opt), argv[i])
                i += 1

        else:
            args.append(parse_token(argv[i]))
            i += 1

    return args, kwargs

