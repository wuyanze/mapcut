""" Handles reaction strings and iterations.
"""

from . import unit

EQU_SYMBOL = '='
PLUS_SYMBOL = '+'

def to_reaxstr(reactants, products):
    """ Encoding reaction string
    """
    return PLUS_SYMBOL.join(reactants) + EQU_SYMBOL + PLUS_SYMBOL.join(products)


def split_reaxstr(reaxstr):
    """ Split reaction string into reactants, products. Spaces are not allowed.
    """
    rs = reaxstr.split(EQU_SYMBOL, 1)
    return rs[0].split(PLUS_SYMBOL), rs[1].split(PLUS_SYMBOL)


def canon_reaxstr(reaxstr, output_reversed=False):
    """ Canonicalize reaction string. Species are ranked from alphabetical order.
    """
    reactants, products = split_reaxstr(reaxstr)
    reactants.sort()
    products.sort()
    reverse = products < reactants
    if reverse:
        reactants, products = products, reactants
    if output_reversed:
        return to_reaxstr(reactants, products), reverse
    else:
        return to_reaxstr(reactants, products)


def canon_reaxstr_without_reverse(reaxstr):
    reactants, products = split_reaxstr(reaxstr)
    reactants.sort()
    products.sort()   
    return to_reaxstr(reactants, products)


def is_canonical(reaxstr):
    return reaxstr == canon_reaxstr(reaxstr)


def rev_reaxstr(reaxstr):
    """ Reverse reaction string (without canonicalize)
    """
    equal_idx = reaxstr.index(EQU_SYMBOL)
    return reaxstr[equal_idx + 1:] + EQU_SYMBOL + reaxstr[:equal_idx]


def species_set(reactions):
    """ Getting species from reaction list
    """
    species = set()
    for r in reactions:
        reactants, products = split_reaxstr(r)
        species.update(reactants)
        species.update(products)
    return sorted(species)


class Reaction:
    def __init__(self, reaxstr=None, k=None, **kwargs):
        if not reaxstr:
            self.reactants = []
            self.products = []
        else:
            self.reactants, self.products = split_reaxstr(reaxstr)

        if k:
            self.k = k
        else:
            self.k = (0.0, 0.0)

        self.params = kwargs
        self.unit = unit.ATOMIC
    
    def __getitem__(self, index):
        return self.params[index]

    def __repr__(self):
        return to_reaxstr(self.reactants, self.products)

    def __setitem__(self, index, attr):
        self.params[index] = attr

    def __eq__(self, other):
        if isinstance(other, str):
            return str(self) == other
        elif isinstance(other, Reaction):
            return self.reactants == other.reactants and self.products == other.products
        else:
            raise ValueError(other)

    def canonicalize(self):
        self.reactants.sort()
        self.products.sort()
        if self.products < self.reactants:
            self.reactants, self.products = self.products, self.reactants
            self.k = (self.k[1], self.k[0])

    def std_equ_constant(self):
        """ Returns standard equlibrium constant.
        """
        if self.unit == unit.SI:
            return self.k[1] / self.k[0]
        else:
            return self.k[1]/self.k[0] * unit.c_2_si ** (len(self.reactants) - len(self.products))

    def to_si(self):
        """ Change unit of kinetic constant to SI unit.
        """
        if self.unit == unit.SI:
            pass
        else:
            self.k = (
                self.k[0] / (unit.c_2_si ** (len(self.reactants)-1) * unit.time_2_si),
                self.k[1] / (unit.c_2_si ** (len(self.products)-1) * unit.time_2_si)
                )
            self.unit = unit.SI
