""" Provide r/w for block structured file.
"""

import sys 

from .iohandle import strip_comment

def ReadBlockFile(filename, blocks, split_char='\t', comment_char='#'):
    """ Read a 'block file' while block are separated by upper-case letters.
    Arguments:
        filename: input file name.
        blocks: contains the block title. Last one is block end;
        split_char: Char used in split within line;
        comment_char: Char used to separate comment;
    Returns:
        Return a dict, key is block name, value is list containing tuples (name, [args]);
    Errors:
        IndexError: Block not found.
    """
    output = dict()
    with open(filename) as f:
        lines = f.readlines()
    currentBlock = None

    for l in lines:
        l = strip_comment(l, comment_char=comment_char)
        if not l:
            continue
        splits = l.split(split_char)
        if len(splits) == 1:
            try:
                blockIndex = blocks.index(splits[0])
            except ValueError:
                pass
            else:
                if not blockIndex == len(blocks) - 1:
                    currentBlock = splits[0]
                    output[currentBlock] = []
                else:
                    currentBlock = None
                continue

        if not currentBlock:
            raise IndexError('Block %s Not Found' % splits[0])
        if len(splits) > 1:
            output[currentBlock].append((splits[0], splits[1:]))
        else:
            output[currentBlock].append((splits[0], []))

    return output      


def WriteBlockFile(filename, blocks, split_char='\t', end_block='END'):
    """
        Write block file.
    Args:
        filename: The output filename (None to use stdout)
        blocks: dict, keys are block names, values are lists containing tuple (name, arg)
    """
    if filename is not None:
        file = open(filename, 'w')
    else:
        file = sys.stdout 

    for block, lines in blocks.items():
        file.write(block + '\n')
        for line in lines:
            if not isinstance(line, tuple):
                file.write('%s\n' % str(line))
            else:
                assert len(line) == 2, 'Invalid block line in block [len(tuple)!=2]'
                if not line[1]:
                    file.write('%s\n' % line[0])
                else:
                    file.write('%s%c%s\n' % (str(line[0]), split_char, 
                                        split_char.join(str(arg) for arg in line[1])))

        file.write(end_block + '\n')

    if file is not sys.stdout:
        file.close()
