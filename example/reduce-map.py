import sys
sys.path.append('..')

from mapcut import reaxmap
from mapcut import reduce
from mapcut.stats import dump

if __name__ == '__main__':  # avoid error on windows

    rmap = reaxmap.read_file('rmap.rkd')
    rmap.set_priority(['[HH]', 'OO', '[OH2]'], reaxmap.Priority.CRITICAL)
    rawdata = dump.read_file('dump2-c.csv', 'dump2-r.csv')
    rsample = rawdata.sample(50)
    rmap_red, rmap_red_series = reduce.reduce_map(rmap, rsample, 'drgep', 0.2, np=1)
    rmap_red.write_file('rmap_red.rkd')