import sys
sys.path.append('..')

from mapcut.stats import dump
from mapcut.stats import ratelaw

rawdata = dump.read_file('dump-c.csv', 'dump-r.csv')
rstat = ratelaw.ReaxStat()
rstat.load(rawdata)
rstat.submit() # end of loading data
rmap = rstat.to_reaxmap(volume=512000)  # system volume in A^3
rmap.write_file('rmap.rkd')