import sys

sys.path.append('..')

from mapcut import reaxmap
from mapcut import maprun

rmap = reaxmap.read_file('rmap.rkd')
runner = maprun.MapRunner()
runner.load(rmap)
runner.set_c0({'[HH]':7.8e-4, 'OO':3.9e-4})   # initial concentration
runner.set_dump(filename='dump2-c.csv', filename_r='dump2-r.csv', interval=1) # concentration and rate dump files
runner.run(time=100, timestep=1)
runner.write_restart('restart.txt')