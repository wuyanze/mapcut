export PYTHONPATH=$PYTHONPATH:..

python -m mapcut sample -i 50 -o sample2.csv dump2-c.csv dump2-r.csv
python -m mapcut reduce -a 0.2 -e drgep -s sample2.csv -o rmap_red.rkd -k [HH],OO,[OH2] rmap.rkd