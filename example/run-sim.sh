export PYTHONPATH=$PYTHONPATH:..

python -m mapcut run -i 1 -t 100 -s 1.0 -b [HH]=7.8E-4,OO=3.9e-4 --dump=dump2-c.csv --rdump=dump2-r.csv rmap.rkd