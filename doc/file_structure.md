# File Structure Format
Structure of files used by Mapcut package.

## Reaction Kinetic Data File
Text file that stores core kinetic information and rate constants in a reaction system.

__Line Comment__: Starting with "\#";

__Handler__: [reaxmap.py](../mapcut/core/reaxmap.py)

### Species Block
- Start: "SPECIES";
- Species: String, separated by space, line or tab;
- End: "END";

### Reaction Block
- Start: "REACTIONS";
- Reactions (Items separated by space or tab):
	- Name: string, species separated by "+", equation separated by "=". Space is allowed.
	- Production Rate Constant: double;
	- Consumption Rate Constant: double;
- End: "END"

(__Warning__: Units are not specified in rkd file. Defaults are A, au, ps.)

### Example
```
SPECIES
H2
O2
H2O
END
REACTIONS
H2+H2+O2=H2O+H2O	1.5E+15	1.0E+12
# comment
END
```

## Reaction Sample File
A formatted CSV file recording statistics of species concentration and reaction rates. Used by reduction and network analysis.

__Line Comment__ : Starting with '\#'.

__Seperator__: Default is comma (',').

__Handler__: [reaxsample.py](../mapcut/core/reaxsample.py)

### Concentration
- Begin: One line, "SPECIES";
- Species name: One line, begin with "t", seperated by separator;
- Concentration: Each one line, begin with time, seperated by separator;
- End: "END";

### Reaction Rate
- Begin: One line, "REACTIONS";
- Reaction name: One line, begin with "t", seperated by separator;
- Reaction rate: Each one line, begin with time, seperated by separator;
- End: "END";

### Initial Concentration
- Begin: One line, "INIT";
- Initial concentration: Each one line, contains "[Species name],[Concentration]";
- End: "END";

The initial concentration is optional, but may cause error in some analyzation, such as production flow analyzation.

### Example
```
SPECIES
t,H2,O2,H2O
0,2.0,1.0,0.0
1,1.8,0.9,0.2
END
REACTIONS
t,H2+H2+O2=H2O+H2O
0,0.055
1,0.045
END
INIT
H2,2.0
O2,1.0
END
```

## Reaction Data Dump Files
Raw dump files of species concentration and reaction rates. Both are in CSV format.

__Seperator__: Default is comma (',').

__Handler__: [dump.py](../mapcut/stats/dump.py)

#### Concentration dump file

A large table, index is time, head line is species. Topleft corner is 't'.

#### Reaction rates dump file

A large table, index is time, head line is reaction names (also reverse reaction names). Topleft corner is 't'.

The rates dump file often appears at the same time with concentration dump, otherwise it may cause error in some analyzation (such as rate constant calculation).

## Kinetic Restart File 
Restart file used by kinetic simulation.

__Handler__: [maprun.py](../mapcut/maprun.py)

### Structure

First row: 't=[time]';

Other rows: '[species]=[concentration]'

### Example
``` 
t=1
H2=1.8
O2=0.9
```