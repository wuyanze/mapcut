# Mapcut Shell Commands

Mapcut also can be directly executed as a shell command, which can achieve most functions inside the package.

If you have not set mapcut as a shell command, ``mapcut`` here represents ``python -m mapcut``. You can also save this command as a shell script:

    $ cd "environment_path_you_like"
    $ echo 'python -m mapcut $\*' > mapcut
    $ chmod +x mapcut

The formal symnopsis of `mapcut` is

    mapcut [COMMAND] [ARG]...

Available commands are:

- diff: Perform difference on mechanism files or data files.
- merge: Merge mechanism files or data files.
- rate: Calculate rate constants based on dump files.
- reduce: Reduce mechanism.
- run: Calculate time evolution of rates based on ODEs.
- sample: Perform sampling on dump files.
- show: Production flow analysis and network file dump.

Details of each sub-command are listed below.

## mapcut diff

Detect and output the differences between several objects.
- If task is `ReaxMap`, output a report of differences in species and reactions between multiple reaction maps.
- If task is `ReaxSample`, calculate and output the deviation of species concentration.

Source: [stats/diff.py](../mapcut/stats/diff.py)

Usage:

    mapcut diff [-m/-s] (-o output) [FILE]...

Arguments:

- -m: specify `ReaxMap` as merge object;
- -s: specify `ReaxSample` as merge object;
- -o [output]: Output filename (default is `stdout`)

FILE:

Input filename depending on task type. If task type is `ReaxMap`, [Reaction Kinetic Data File (\*.rkd)](file_structure.md#reaction-kinetic-data-file) is required. If task type is `ReaxSample`, [Reaction Sample File](file_structure.md#reaction-sample-file) is required.

## mapcut merge

Merge multiple objects together.
- If task is `ReaxMap`, combine all species and reactions and get into a larger reaction map.
- If task is `ReaxSample`, averaging species concentration and adding reaction rates.

Source: [stats/merge.py](../mapcut/stats/merge.py)

Usage: 

    mapcut merge [-s/-m] (-o output) [FILE]...

Arguments:

- -m: Specify `ReaxMap` as merge object;
- -s: Specify `ReaxSample` as merge object;
- -o [output]: Output filename, default is stdout;

FILE:

Input filename depending on task type. If task type is `ReaxMap`, [Reaction Kinetic Data File (\*.rkd)](file_structure.md#reaction-kinetic-data-file) is required. If task type is `ReaxSample`, [Reaction Sample File](file_structure.md#reaction-sample-file) is required.

## mapcut rate

Calculate rate constants from reaction data, and output to a reaction kinetic data file. If `--simple` is not given, also output a analyzation report to a file.

Source: [stats/rateconst.py](../mapcut/stats/rateconst.py)

Usage:

    mapcut rate [-v volume] (-c config) (-o output) (--overlay=) (--simple) (--kwd=value...) [FILE_C] [FILE_R] ...

Arguments:

- -v [volume]: System volume;
- -c [config]: Configure file recording volume and time interval;
- -o [output]: Output filename (default is `stdout`);
- --overlay=[overlay]: Overlay style of samples, can be 'para' or 'seq';
- --simple: Do not output report file;

Keyword Arguments:

Additional arguments will be passed to [ReaxStat.to_reaxmap()](../mapcut/stats/ratelaw.py).

FILE_C, FILE_R:

Must be name of concentration dump and reaction rate dump files __in turn__. See [file structures](file_structure.md#reaction-data-dump-files) for details.

## mapcut reduce

Perform reduction of reaction systems.

Source: [ui_reduce.py](../mapcut/ui_reduce.py)

Usage:

    mapcut reduce [OPTION] ... [FILE] ...

OPTION:

- -a, --args: Arguments that passed to reduction method.
- -s, --sample: Reaxsample file. Only one sample is allowed.
- -k, --keep: Species to keep. Used in DRG-like methods.
- -o, --output: Filename of reduced reaction kinetic data file.
- -e, --method: Method used;
- -t, --time: Interval of each reaxmap that holds within, if there are multiple maps.
- --auto: Automatic reduction.
- --output_sample: Filename to write reaxsample that returned in automatic reduction.

Additional options will be passed to reduction methods as keyword arguments.

FILE:

Reaction kinetic data files (\*.rkd). See [file structures](file_structure.md#reaction-data-dump-files) for details.

## mapcut run

Compute a reaction system by solving ODEs.

Source: [ui_run.py](../mapcut/ui_run.py)

Usage:

    mapcut run [OPTION] ... [FILE] ...

OPTION:

- -s, --timestep: Timestep for running.
- -b, --begin: Begin concentration or restart file.
- -t, --time: Total time for running, if there are one map.
- -i, --interval: Interval of each reaxmap that holds within, if there are multiple maps.
- -o, --dump: Dump file of concentration.
- --rdump: Dump file of reaction rates.
- --restart: Restart filename.
- --dump_interval: Interval for dump.

Additional options will be passed to `MapRunner.run()` and `MapRunner.set_dump()`.

FILE:

Reaction kinetic data files (\*.rkd). See [file structures](file_structure.md) for details.

## mapcut sample

Generate a Reaction Sample File from Reaction Data Dump Files.

Source: [stats/sample.py](../mapcut/stats/sample.py)

Usage:

    mapcut sample [-i sample_interval] (-r sample_range) [-o output] (-v volume) (--rev) [FILE_C] [FILE_R]

Arguments:

- -i [sample_interval]: Frame number between sampling;
- -r [sample_range]: Number of sample points count around central point;
- -o [output]: Output filename (default is `stdout`);
- -v [volume]: Divide volume for each concentration and rate data;
- --rev: Treat reversible reaction as two seperated ones.

FILE_C, FILE_R:

Concentration dump filename and rate dump filename. See [file structures](file_structure.md#reaction-data-dump-files) for details.

## mapcut show

Generating graph representation of a reaction network and investigating flux of species and elements in a reaction system.

Source: [ui_graph.py](../mapcut/ui_graph.py)

Usage:

    mapcut show [OPTION] ... [FILE] (FILE2)

OPTION:

- --abs: If set, dump absolute value rather than relative value;
- -e [element]: Select element to calculate path flow. Default is `mass`.
- -i: Set to enter interact mode;
- -m [reaxmap]: Filename of reaxmap. If set, only use reactions and species described in the map;
- -s [slice]: Slice string, such as '1:2'. Slice sample or rawdata;
- -o [output]: Output filename. Default is stdout;
- --rev: Treat reversible reaction as two seperated reactions;

FILE, FILE2

If only one filename is given, treat it as a [Reaction Sample File](file_structure.md#reaction-sample-file). If two filenames are given, treat them as [Reaction Data Dump Files](file_structure.md#reaction-data-dump-files).

Interacting mode arguments:

- dump [filename]: Dump json data to file;
- element [element]: Rebuild flow graph based on element;
- group [element] (group_id): Assign a group to species with specific element;
- info [species_name]: Get species information;
- reaction [species1] [species2]: Get reaction information between two species.
- refresh: Rebuild flow graph;
- select [species...]: Select a species to group;
- ungroup: Remove all groups.
