# Mapcut
A postprocessing tool for Reactive MD simulation in Python 3.

This is a part of ReaxSim project, which aims at automatic mechanism analyzation of chemical reaction simulation (AIMD, ReaxFF MD) trajectory.

## Documentation

Documentation of Mapcut package is [here](doc/user.md).

For avaliable argments of shell commands, see [here](doc/mapcut.md).

## Installation

You can install the package via ``pip``:
```
pip install -e mapcut
```

Alternatively, set environment variable ``PYTHONPATH`` to the package root also works.

Package Prerequisties:

- Python >= 3.0
- numpy >= 1.13
- scipy >= 0.19
- pandas >= 0.22
- networkx >= 2.1

If you want to use mapcut as a shell command, you may create a script:
```
printf '#!/bin/sh\npython3 -m mapcut "$@"' > mapcut
chmod +x mapcut
mv mapcut ~/bin/
```

## Citing

If you use `mapcut` in a scholarly work, please cite:

[Wu, Y., Sun, H., Wu, L., & Deetz, J. D. (2019). Extracting the mechanisms and kinetic models of complex reactions from atomistic simulation data. *Journal of computational chemistry.*](https://doi.org/10.1002/jcc.25809)

If you use DRG reduction method inside `mapcut`, please also cite:

- [Lu T. F. , Law C. K. , Proc. Combust. Inst., 30:1333--1341, 2005.](https://doi.org/10.1016/j.proci.2004.08.145)
- [Niemeyer, K. E.; Sung, C. J.; Raju, M. P., Combust. Flame, 157(9):1760--1770, 2010.](https://doi.org/10.1016/j.combustflflame.2009.12.022)
- [Niemeyer, K. E.; Sung, C. J., Combust. Flame, 158(8):1439--1443, 2011.](https://doi.org/10.1016/j.combustflflame.2010.12.010)
- [Niemeyer, K. E.; Sung, C. J., Combust. Flame, 161(11):2752--2764, 2014.](https://doi.org/10.1016/j.combustflame.2014.05.001)

If you use DRGEP reduction method inside `mapcut`, please also cite:

- [Pepiot-Desjardins P. , Pitsch H. , Combust. Flame, 154:67--81, 2008.](https://doi.org/10.1016/j.combustflame.2007.10.020)
- [Niemeyer, K. E.; Sung, C. J.; Raju, M. P., Combust. Flame, 157(9):1760--1770, 2010.](https://doi.org/10.1016/j.combustflflame.2009.12.022)

If you use CSPI reduction method inside `mapcut`, please also cite:

- [Lu T. F. , Law C. K. , Combust. Flame, 154:153--163, 2008.](https://doi.org/10.1016/j.combustflame.2007.11.013)
